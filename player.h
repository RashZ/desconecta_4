#ifndef PLAYER_H
#define PLAYER_H

#include "environment.h"

class Player{
    public:
      Player(int jug);
      Environment::ActionType Think();
      void Perceive(const Environment &env);
    private:
      int jugador_;
      Environment actual_;
      double PODA_ALFA_PETA(const Environment &tablero,int jug,int prof,int limite,Environment::ActionType &accion,double alpha,double beta);
     // double Heuristica(const Environment &estado, int jugador);


};
#endif
