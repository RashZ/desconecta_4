#include <iostream>
#include <cstdlib>
#include <vector>
#include <queue>
#include "player.h"
#include "environment.h"

using namespace std;

const double masinf=9999999999.0, menosinf=-9999999999.0;


// Constructor
Player::Player(int jug){
    jugador_=jug;
}

// Actualiza el estado del juego para el jugador
void Player::Perceive(const Environment & env){
    actual_=env;
}

double Puntuacion(int jugador, const Environment &estado){
    double suma=0;

    for (int i=0; i<7; i++)
      for (int j=0; j<7; j++){
         if (estado.See_Casilla(i,j)==jugador){
            if (j<3)
               suma += j;
            else
               suma += (6-j);
         }
      }

    return suma;
}


// Funcion de valoracion para testear Poda Alfabeta
double ValoracionTest(const Environment &estado, int jugador){
    int ganador = estado.RevisarTablero();

    if (ganador==jugador)
       return 99999999.0; // Gana el jugador que pide la valoracion
    else if (ganador!=0)
            return -99999999.0; // Pierde el jugador que pide la valoracion
    else if (estado.Get_Casillas_Libres()==0)
            return 0;  // Hay un empate global y se ha rellenado completamente el tablero
    else
          return Puntuacion(jugador,estado);
}

// ------------------- Los tres metodos anteriores no se pueden modificar



// Funcion heuristica (ESTA ES LA QUE TENEIS QUE MODIFICAR)
double Heuristica(const Environment &estado, int jugador){




   int JUG_ACTIVO_horizontales_no_se_completan = 0 , JUG_ACTIVO_verticales_no_se_completan = 0 , JUG_ACTIVO_diagonales_no_se_completan = 0 ,
       JUG_ADVERSARIO_horizontales_no_se_completan = 0 , JUG_ADVERSARIO_verticales_no_se_completan = 0 , JUG_ADVERSARIO_diagonales_no_se_completan = 0 ;

  // int resultado , JUG_1_CAMINOS_NO_SE_COMPLETAN , JUG_2_CAMINOS_NO_SE_COMPLETAN  ;
   /*
        * Pienso en hacer la heuristica de la siguiente forma :
            Para cada tablero calculo el numero de filas , columnas y diagonales que no se pueden completar tanto para el jugador activo como
            para el jugador adversario .

        * Sabiendo que el valor de la heuristica ser� :
            ValorFinal = sumaJugAdversario(filas,columnas,diagonales) - sumaJugActivo( filas,columnas,diagonales) ;

            Nota : filas,columnas,diagonales "los que se pueden completar"

            * Con una condici�n de Parada : encontrar una fila o columna o diagonal completo para el JugadorActivo :
                " 4 casillas adyacentes " .

        * Mecanismo Principal :
            Principalmente Tenemos en cuenta la ocupaci�n de cada columna y comparamos la casilla que est� en el tope de la columna ,
            y para esta casilla la voy a llamar "Casilla_Actual" , hacemos 2 tipos de comparaciones :


                    1. * Valoraci�n de Adyacencia :
                            * Aqui se cuenta el numero de casillas adyacente a Casilla_Actual en " Vertical , Horizontal , Diagonal " :
                                4 casillas adyacentes --> P�rdida para el jugador
                                3 casillas adyacentes --> 75
                                2 casillas adyacentes --> 50

                    2. * Valoraci�n de del numero de Columnas , Horizontales , Verticales que se pueden completar para un jugador
                         desde Casilla_Actual  teniendo en cuenta las casillas adyacentes a Casilla_Actual :
                            * Un Camino que se puede Completar apartir de una secuencia formada por 3 casillas : 75
                            * Un Camino que se puede Completar apartir de una secuencia formada por 2 casillas : 50
                            * Un Camino que se puede completar apartir de una secuencia formada por 1 casilla : 25

                    ***  Caso " Hay Bomba Para el Jugador Adversario " >> Se calcula en un tablero auxiliar el resultado de explotar la bomba ,
                        y se calcula el valor de heuristica para este nuevo tablero auxiliar :
                            * Si implica perdida para el jugador Activo >> se pone el valor del tablero principal a -1000000

                            * Si el valor de heuristica del tablero auxiliar es menor que el valor del tablero principal >>
                              Valor_Heuristica_Tablero_Principal = min( Valor_Heuristica( Tablero_Principal )  , Valor_Heuristica(Tablero_Auxiliar) )

   */

    Environment estado_aux ;

    double resultado=0;
    int adyacentes_MAX = 0 , adyacentes_MIN = 0 , col_aux = 0 , i;
    bool stop_contar_adyacentes = false , stop_asi_pierdo = false , stop_asi_gano = false , jugador_adversario_tiene_bomba = false , solo_cuenta_casillas_libres = false ;
    int adyacente_de_3_MAX = 0 , adyacente_de_3_MIN = 0 , adyacente_de_2_MAX = 0 , adyacente_de_2_MIN = 0 ,
        Caminos_a_partir_de_3_MAX = 0 , Caminos_a_partir_de_3_MIN = 0 , Caminos_a_partir_de_2_MAX = 0 , Caminos_a_partir_de_2_MIN = 0 ,
        Caminos_a_partir_de_1_MAX = 0 , Caminos_a_partir_de_1_MIN = 0 , Parte_Activo , Parte_Adversario , Jugador_Min ,
        aux_adyacente = 0 , casillas_libres = 0 ;

    /* Identificar al jugador adversario  */
    Jugador_Min = jugador+1 ;
    if(jugador == 2){
        Jugador_Min = 1;
    }


    for(int j = 0 ; j<7 && ( !stop_asi_gano && !stop_asi_pierdo ) ; j++){
         //   cout<<"FIRST"<<endl;
            solo_cuenta_casillas_libres = false;
            stop_contar_adyacentes=false; //asignar de nuevo a false "se va a utilizar de nuevo"
            col_aux = j;
            casillas_libres = 0;
            aux_adyacente = 0;
            if(estado.Get_Ocupacion_Columna(j) != 0){
                 i = estado.Get_Ocupacion_Columna(j) - 1 ;


                 if(  estado.See_Casilla(i,j) == jugador || estado.See_Casilla(i,j) == jugador+3 ){
                /*
                    contar los adyacentes en vertical
                */
                 //   cout<<"Estoy en Jugador Max , Vertical"<<endl;

                    if(estado.Get_Ocupacion_Columna(j) >1 ){
                        for(i = estado.Get_Ocupacion_Columna(j)-1 ; i>=0  && !stop_contar_adyacentes ; i--){
                            if( estado.See_Casilla(i,j) == jugador || estado.See_Casilla(i,j) == jugador+3 ){ //
                                aux_adyacente++;
                            }
                            else{
                                stop_contar_adyacentes = true;
                            //    cout<<aux_adyacente<<endl;
                            }
                        }
                   //     cout<<"Columna : "<<j<<" adyacentes : "<<aux_adyacente<<endl;
                        //contar las posibilidades de comletar 4 casillas seguidas
                      //  i = estado.Get_Ocupacion_Columna(j) - 1;


                    }
                    if( ( (aux_adyacente) + ( 7 - estado.Get_Ocupacion_Columna(j) ) ) >= 4  ){
                            //adyacentes_MAX += (4-aux_adyacente)*20;
                        if( (aux_adyacente) == 1){
                            Caminos_a_partir_de_1_MAX++;
                        }
                        else if( (aux_adyacente) == 2){
                            Caminos_a_partir_de_2_MAX++;
                        }
                        else if( (aux_adyacente) == 3){
                            Caminos_a_partir_de_3_MAX++;
                        }
                    }

                    if( (aux_adyacente) >= 4){
                            cout<<"PIERDO en columna "<<j<< "de MAX VERTICAL"<<endl;
                            stop_asi_pierdo = true;
                          //  cout<<"Pierdo"<<endl;
                    }
                    else if( aux_adyacente != 0){
                        if( (aux_adyacente) == 2){
                            adyacente_de_2_MAX++;
                        }
                        else if( (aux_adyacente) == 3){
                            adyacente_de_3_MAX++;
                        }
                      //  adyacentes_MAX += (aux_adyacente+1)*10;
                        aux_adyacente = 0;
                    }

                    stop_contar_adyacentes=false; //asignar de nuevo a false "se va a utilizar de nuevo"
                    col_aux = j;
                    if(estado.Get_Ocupacion_Columna(j) != 0){
                        i = estado.Get_Ocupacion_Columna(j) - 1 ;
                    }
                            //contar los adyacentes en horizontal

                            //Caso de que la casilla comprobada es un extremo " 6 � 0 "

                    if(j == 0 || j == 6 && ( !stop_asi_gano && !stop_asi_pierdo ) ){ //caso de extremos , pues se ahorra una comparaci�n
                        aux_adyacente = 0;
                        if(j == 0){
                        //    cout<<"Estoy en Jugador Max , 0 "<<endl;
                            for(col_aux = j+1 ; col_aux < 4 && !stop_contar_adyacentes ; col_aux++){
                                if( ( ( estado.See_Casilla(i,col_aux) == jugador ) || ( estado.See_Casilla(i,col_aux) == jugador+3 ) )  &&
                                     !solo_cuenta_casillas_libres
                                   ){ //
                                        aux_adyacente++;
                                }
                                else{
                                    if(estado.See_Casilla(i,col_aux) == 0){
                                        solo_cuenta_casillas_libres = true;
                                        casillas_libres++;
                                    }
                                    else{
                                        stop_contar_adyacentes = true;
                                    }

                                }
                            }

                        }
                        else{
                        //    cout<<"Estoy en Jugador Max , 6 "<<endl;
                            for(col_aux = j-1 ; col_aux > 2 && !stop_contar_adyacentes ; col_aux--){
                                if( ( ( estado.See_Casilla(i,col_aux) == jugador ) || (estado.See_Casilla(i,col_aux) == jugador+3 ) )  &&
                                     !solo_cuenta_casillas_libres ){ //
                                    aux_adyacente++;
                                }
                                else{
                                    if(estado.See_Casilla(i,col_aux) == 0){
                                        solo_cuenta_casillas_libres = true;
                                        casillas_libres++;
                                    }
                                    else{
                                        stop_contar_adyacentes = true;
                                    }

                                }
                            }

                        }
                        if(  (aux_adyacente+1)<4 &&   (aux_adyacente+1)+casillas_libres>= 4  ){
                                //adyacentes_MAX += casillas_libres*10;
                            if(aux_adyacente+1 == 1){
                                Caminos_a_partir_de_1_MAX++;
                            }
                            else if(aux_adyacente+1 == 2){
                                Caminos_a_partir_de_2_MAX++;
                            }
                            else if(aux_adyacente+1 == 3){
                                Caminos_a_partir_de_3_MAX++;
                            }
                        }

                        if( (aux_adyacente+1) >= 4 ){
                            cout<<"PIERDO en columna "<<j<< "de MAX HORIZONTAL"<<endl;
                            stop_asi_pierdo = true;
                           // cout<<"Pierdo"<<endl;
                        }
                        else if( aux_adyacente != 0){
                           // adyacentes_MAX += (aux_adyacente+1)*10;
                           // aux_adyacente = 0;
                            if(aux_adyacente+1 == 2){
                                adyacente_de_2_MAX++;
                            }
                            else if(aux_adyacente+1 == 3){
                                adyacente_de_3_MAX++;
                            }
                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                            aux_adyacente = 0;
                        }
                        /***  Calcular las secuencias diagonales Para j = 6 , j = 0  ***/

                        if(!stop_asi_pierdo){
                            aux_adyacente = 0;
                            casillas_libres = 0;
                            if( estado.Get_Ocupacion_Columna(j) > 0)
                                i = estado.Get_Ocupacion_Columna(j) - 1 ;
                            solo_cuenta_casillas_libres = false;
                            stop_contar_adyacentes = false;
                            int fila_diagonal;
                            int columna_diagonal ;

                            if(j == 0 && estado.Get_Ocupacion_Columna(j) > 0){

                                        if(i>=0 && i<=3){
                                            fila_diagonal = i+1;
                                            columna_diagonal = j+1;
                                            while( (fila_diagonal <= i+3 && columna_diagonal <= j+3) && !stop_contar_adyacentes ){

                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                      estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) &&
                                                     !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal++;
                                                        columna_diagonal++;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal++;
                                                        columna_diagonal++;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }
                                        }
                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MAX++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"PIERDO en columna "<<j<< "de MAX DIAGONAL"<<endl;
                                            stop_asi_pierdo = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MAX++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }


                                        if( (i>=3 && i<=6) && !stop_asi_pierdo ){
                                         //   cout<<"estoy dentro"<<endl;
                                            fila_diagonal = i-1; //incializamos la fila a una fila menos en caso de trabajar con filas de 3 a 6
                                          //  cout<<fila_diagonal<<endl;
                                            columna_diagonal = j+1; //volvemos a inicializar la columna
                                            casillas_libres = 0;
                                            aux_adyacente = 0;
                                            stop_contar_adyacentes = false;
                                            solo_cuenta_casillas_libres = false;
                                            while( (fila_diagonal <= i+3 && columna_diagonal <= j+3) && !stop_contar_adyacentes ){
                                                  //  cout<<"whileee"<<endl;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) &&
                                                         !solo_cuenta_casillas_libres)
                                                    {
                                                            aux_adyacente++;
                                                            fila_diagonal--;
                                                            columna_diagonal++;

                                                    }
                                                    else{
                                                        if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            solo_cuenta_casillas_libres = true;
                                                            casillas_libres++;

                                                            fila_diagonal--;
                                                            columna_diagonal++;
                                                        }
                                                        else{
                                                            stop_contar_adyacentes = true;
                                                        }
                                                    }

                                            }
                                        }

                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MAX++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"PIERDO en columna "<<j<< "de MAX DIAGONAL"<<endl;
                                            stop_asi_pierdo = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MAX++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }
                                  } //if(j == 0)
                                  else if(j == 6 ){
                                        if(i>=0 && i<=3){
                                            fila_diagonal = i+1;
                                            columna_diagonal = j-1;
                                            while( (fila_diagonal <= i+3 && columna_diagonal >= j-3) && !stop_contar_adyacentes ){

                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                      estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) &&
                                                     !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal++;
                                                        columna_diagonal--;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal++;
                                                        columna_diagonal--;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }
                                        }
                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MAX++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"PIERDO en columna "<<j<< "de MAX DIAGONAL"<<endl;
                                            stop_asi_pierdo = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MAX++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }


                                        if( (i>=3 && i<=6) && !stop_asi_pierdo ){
                                         //   cout<<"estoy dentro"<<endl;
                                            fila_diagonal = i-1; //incializamos la fila a una fila menos en caso de trabajar con filas de 3 a 6
                                          //  cout<<fila_diagonal<<endl;
                                            columna_diagonal = j-1; //volvemos a inicializar la columna
                                            casillas_libres = 0;
                                            aux_adyacente = 0;
                                            stop_contar_adyacentes = false;
                                            solo_cuenta_casillas_libres = false;
                                            while( (fila_diagonal >= i-3 && columna_diagonal >= j-3) && !stop_contar_adyacentes ){
                                                  //  cout<<"whileee"<<endl;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) &&
                                                         !solo_cuenta_casillas_libres)
                                                    {
                                                            aux_adyacente++;
                                                            fila_diagonal--;
                                                            columna_diagonal--;

                                                    }
                                                    else{
                                                        if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            solo_cuenta_casillas_libres = true;
                                                            casillas_libres++;

                                                            fila_diagonal--;
                                                            columna_diagonal++;
                                                        }
                                                        else{
                                                            stop_contar_adyacentes = true;
                                                        }
                                                    }

                                            }
                                        }

                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MAX++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"PIERDO en columna "<<j<< "de MAX DIAGONAL"<<endl;
                                            stop_asi_pierdo = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MAX++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }

                                  }
                            } //if(!stop_asi_pierdo) //calcular los diagonales

                    }// if(j == 0 || j == 6 && ( !stop_asi_gano && !stop_asi_pierdo ) ){
                            //caso de un casilla adyavente a uno de los extremos
                    else if(j == 1 || j == 5 && ( !stop_asi_gano && !stop_asi_pierdo ) ){
                        aux_adyacente = 0;
                        if(j == 1){
                        //    cout<<"Estoy en Jugador Max , 1 "<<endl;
                            if( estado.See_Casilla(i,j-1) == jugador || estado.See_Casilla(i,j-1) == jugador+3 ){
                                aux_adyacente++;
                            }
                            else if(estado.See_Casilla(i,j-1) == 0){
                                    casillas_libres++;
                            }
                            for(col_aux = j+1 ; col_aux<5 &&  !stop_contar_adyacentes ; col_aux++){
                                if( ( ( estado.See_Casilla(i,col_aux) == jugador ) || ( estado.See_Casilla(i,col_aux) == jugador+3 ) )  &&
                                    !solo_cuenta_casillas_libres){
                                    aux_adyacente++;
                                }
                                else{
                                    if(estado.See_Casilla(i,col_aux) == 0){
                                        solo_cuenta_casillas_libres = true;
                                        casillas_libres++;
                                    }
                                    else{
                                        stop_contar_adyacentes = true;
                                    }

                                }
                            }

                        }
                        else{
                       //      cout<<"Estoy en Jugador Max , 5 "<<endl;
                            if( estado.See_Casilla(i,j+1) == jugador || estado.See_Casilla(i,j+1) == jugador+3 ){
                                aux_adyacente++;
                            }
                            for(col_aux = j-1 ; col_aux>1 &&  !stop_contar_adyacentes ; col_aux--){
                                if( ( ( estado.See_Casilla(i,col_aux) == jugador ) || ( estado.See_Casilla(i,col_aux) == jugador+3 ) )  &&
                                    !solo_cuenta_casillas_libres ){
                                    aux_adyacente++;
                                }
                                else{
                                    if(estado.See_Casilla(i,col_aux) == 0){
                                        solo_cuenta_casillas_libres = true;
                                        casillas_libres++;
                                    }
                                    else{
                                        stop_contar_adyacentes = true;
                                    }

                                }
                            }

                        }
                        if(  (aux_adyacente+1)<4 &&   (aux_adyacente+1)+casillas_libres>= 4  ){
                                //adyacentes_MAX += casillas_libres*10;
                            if(aux_adyacente+1 == 1){
                                Caminos_a_partir_de_1_MAX++;
                            }
                            else if(aux_adyacente+1 == 2){
                                Caminos_a_partir_de_2_MAX++;
                            }
                            else if(aux_adyacente+1 == 3){
                                Caminos_a_partir_de_3_MAX++;
                            }
                        }

                        if(aux_adyacente+1 >= 4){
                            cout<<"PIERDO en columna "<<j<< "de MAX HORIZONTAL"<<endl;
                            stop_asi_pierdo = true;
                         //   cout<<"Pierdo"<<endl;
                        }
                        else if(aux_adyacente != 0){
                            //adyacentes_MAX += (aux_adyacente+1)*10;
                            if(aux_adyacente+1 == 2){
                                adyacente_de_2_MAX++;
                            }
                            else if(aux_adyacente+1 == 3){
                                adyacente_de_3_MAX++;
                            }
                            aux_adyacente = 0;
                        }
                        /***  Calcular las secuencias diagonales Para j = 5 , j = 1  ***/

                        if(!stop_asi_pierdo){
                            aux_adyacente = 0;
                            casillas_libres = 0;
                            if( estado.Get_Ocupacion_Columna(j) > 0)
                                i = estado.Get_Ocupacion_Columna(j) - 1 ;
                            solo_cuenta_casillas_libres = false;
                            stop_contar_adyacentes = false;
                            int fila_diagonal;
                            int columna_diagonal ;
                            bool casilla_anterior_comprobada = false;

                            if(j == 5){

                                        if(i>=0 && i<=4){

                                            fila_diagonal = i+1;
                                            columna_diagonal = j-1;
                                            while( ( ( ( i!= 4) && (fila_diagonal <= i+3 && columna_diagonal >= j-3) ) ||
                                                     ( ( i== 4) && (fila_diagonal <= i+2 && columna_diagonal >= j-2) )
                                                    ) &&
                                                   !stop_contar_adyacentes ){

                                                if( ( i != 0 ) && !casilla_anterior_comprobada ){
                                                    fila_diagonal = i-1;
                                                    columna_diagonal = j+1;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                    casilla_anterior_comprobada = true;
                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j-1;
                                                }
                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                      estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) &&
                                                     !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal++;
                                                        columna_diagonal--;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal++;
                                                        columna_diagonal--;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }
                                        }
                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MAX++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"PIERDO en columna "<<j<< "de MAX DIAGONAL"<<endl;
                                            stop_asi_pierdo = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MAX++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }


                                        if( (i>=2 && i<=6) && !stop_asi_pierdo ){
                                            fila_diagonal = i-1; //incializamos la fila a una fila menos en caso de trabajar con filas de 3 a 6
                                            columna_diagonal = j-1; //volvemos a inicializar la columna
                                            casillas_libres = 0;
                                            aux_adyacente = 0;
                                            stop_contar_adyacentes = false;
                                            solo_cuenta_casillas_libres = false;
                                            casilla_anterior_comprobada = false;
                                            while( ( ( ( i != 2 ) && fila_diagonal >= i-3 && columna_diagonal >= j-3 ) ||
                                                     ( ( i == 2 ) && fila_diagonal >= i-2 && columna_diagonal >= j-2 )
                                                    )
                                                     && !stop_contar_adyacentes ){
                                                  //  cout<<"whileee"<<endl;
                                                if( ( i != 6 ) && !casilla_anterior_comprobada ){

                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j+1;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                     casilla_anterior_comprobada = true;
                                                     fila_diagonal = i-1;
                                                     columna_diagonal = j-1;
                                                }
                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                        estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) &&
                                                        !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal--;
                                                        columna_diagonal--;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal--;
                                                        columna_diagonal--;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }
                                        }

                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MAX++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"PIERDO en columna "<<j<< "de MAX DIAGONAL"<<endl;
                                            stop_asi_pierdo = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MAX++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }
                                  } //if(j == 5)
                                  else if(j == 1){
                                       if(i>=0 && i<=4){

                                            fila_diagonal = i+1;
                                            columna_diagonal = j+1;
                                            while( ( ( ( i!= 4) && (fila_diagonal <= i+3 && columna_diagonal <= j+3) ) ||
                                                     ( ( i == 4) && (fila_diagonal <= i+2 && columna_diagonal <= j+2) )
                                                    ) &&
                                                   !stop_contar_adyacentes ){

                                                if( ( i != 0 ) && !casilla_anterior_comprobada ){
                                                    fila_diagonal = i-1;
                                                    columna_diagonal = j-1;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                    casilla_anterior_comprobada = true;
                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j+1;
                                                }
                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                      estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) &&
                                                     !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal++;
                                                        columna_diagonal++;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal++;
                                                        columna_diagonal++;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }
                                        }
                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MAX++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"PIERDO en columna "<<j<< "de MAX DIAGONAL"<<endl;
                                            stop_asi_pierdo = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MAX++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }


                                        if( (i>=2 && i<=6) && !stop_asi_pierdo ){
                                            fila_diagonal = i-1; //incializamos la fila a una fila menos en caso de trabajar con filas de 3 a 6
                                            columna_diagonal = j+1; //volvemos a inicializar la columna
                                            casillas_libres = 0;
                                            aux_adyacente = 0;
                                            stop_contar_adyacentes = false;
                                            solo_cuenta_casillas_libres = false;
                                            casilla_anterior_comprobada = false;
                                            while( ( ( ( i != 2 ) && fila_diagonal >= i-3 && columna_diagonal <= j+3 ) ||
                                                     ( ( i == 2 ) && fila_diagonal >= i-2 && columna_diagonal <= j+2 )
                                                    )
                                                     && !stop_contar_adyacentes ){
                                                  //  cout<<"whileee"<<endl;
                                                if( ( i != 6 ) && !casilla_anterior_comprobada ){

                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j-1;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                     casilla_anterior_comprobada = true;
                                                     fila_diagonal = i-1;
                                                     columna_diagonal = j+1;
                                                }
                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                        estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) &&
                                                        !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal--;
                                                        columna_diagonal++;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal--;
                                                        columna_diagonal++;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }
                                        }

                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MAX++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"PIERDO en columna "<<j<< "de MAX DIAGONAL"<<endl;
                                            stop_asi_pierdo = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MAX++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }
                                  } //if(j == 0)
                            }

                    }
                    else if( (j == 2 || j == 4 ) && ( !stop_asi_pierdo && !stop_asi_gano )){
                        aux_adyacente = 0;
                        if(j == 2){
                        //     cout<<"Estoy en Jugador Max , 2 "<<endl;
                            if( estado.See_Casilla(i,j-1) == jugador || estado.See_Casilla(i,j-1) == jugador+3 ){
                                aux_adyacente++;
                            }
                            if( ( estado.See_Casilla(i,j-1) == jugador || estado.See_Casilla(i,j-1) == jugador+3 ) &&
                              ( estado.See_Casilla(i,j-2) == jugador || estado.See_Casilla(i,j-2) == jugador+3 ) ){
                                aux_adyacente++;
                            }
                            for(col_aux = j+1 ; col_aux<6 &&  !stop_contar_adyacentes ; col_aux++){
                                if( ( ( estado.See_Casilla(i,col_aux) == jugador ) || ( estado.See_Casilla(i,col_aux) == jugador+3 ) )  &&
                                    !solo_cuenta_casillas_libres ){
                                    aux_adyacente++;
                                }
                                else{
                                    if(estado.See_Casilla(i,col_aux) == 0){
                                        solo_cuenta_casillas_libres=true;
                                        casillas_libres++;
                                    }
                                    else{
                                        stop_contar_adyacentes = true;
                                    }

                                }
                            }

                        }
                        else{
                      //      cout<<"estoy en columna 4 de MAX con j = "<<j<<" --fila : "<<i<<endl;
                            i = estado.Get_Ocupacion_Columna(j) -1;
                            aux_adyacente=0;
                        //     cout<<"Estoy en Jugador Max , 4 "<<endl;
                            if( estado.See_Casilla(i,j+1) == jugador || estado.See_Casilla(i,j+1) == jugador+3 ){
                                aux_adyacente++;
                            }
                            if( ( estado.See_Casilla(i,j+1) == jugador || estado.See_Casilla(i,j+1) == jugador+3 ) &&
                                ( estado.See_Casilla(i,j+2) == jugador || estado.See_Casilla(i,j+2) == jugador+3 ) ){
                                aux_adyacente++;
                            }
                            for(col_aux = j-1 ; col_aux>0 &&  !stop_contar_adyacentes ; col_aux--){
                                if( ( ( estado.See_Casilla(i,col_aux) == jugador ) || ( estado.See_Casilla(i,col_aux) == jugador+3 ) )  &&
                                   !solo_cuenta_casillas_libres ){
                                    aux_adyacente++;
                                }
                                else{
                                    if(estado.See_Casilla(i,col_aux) == 0){
                                        solo_cuenta_casillas_libres=true;
                                        casillas_libres++;
                                    }
                                    else{
                                        stop_contar_adyacentes = true;
                                    }
                                }
                            }

                        }
                        if(  (aux_adyacente+1)<4 &&   (aux_adyacente+1)+casillas_libres>= 4  ){
                                //adyacentes_MAX += casillas_libres*10;
                            if(aux_adyacente+1 == 1){
                                Caminos_a_partir_de_1_MAX++;
                            }
                            else if(aux_adyacente+1 == 2){
                                Caminos_a_partir_de_2_MAX++;
                            }
                            else if(aux_adyacente+1 == 3){
                                Caminos_a_partir_de_3_MAX++;
                            }
                        }

                        if(aux_adyacente+1 >= 4){
                            cout<<"PIERDO en columna "<<j<< "de MAX HORIZONTAL"<<endl;
                            stop_asi_pierdo = true;
                          //  cout<<"Pierdo"<<endl;
                        }
                        else if(aux_adyacente != 0){
                            if(aux_adyacente+1 == 2){
                                adyacente_de_2_MAX++;
                            }
                            else if(aux_adyacente+1 == 3){
                                adyacente_de_3_MAX++;
                            }
                            aux_adyacente = 0;
                        }
                        if(estado.Get_Ocupacion_Columna(j) != 0){
                            i = estado.Get_Ocupacion_Columna(j) - 1 ;
                        }
                       if(!stop_asi_pierdo){
                            aux_adyacente = 0;
                            casillas_libres = 0;
                            if( estado.Get_Ocupacion_Columna(j) > 0)
                                i = estado.Get_Ocupacion_Columna(j) - 1 ;
                            solo_cuenta_casillas_libres = false;
                            stop_contar_adyacentes = false;
                            int fila_diagonal;
                            int columna_diagonal ;
                            bool casilla_anterior_comprobada_1 = false;
                            bool casilla_anterior_comprobada_2 = false;
                            if(j == 4){

                                        if(i>=0 && i<=5){

                                            fila_diagonal = i+1;
                                            columna_diagonal = j-1;
                                            while( ( ( ( i!= 4 && i != 5 ) && (fila_diagonal <= i+3 && columna_diagonal >= j-3) ) ||
                                                     ( ( i== 4) && (fila_diagonal <= i+2 && columna_diagonal >= j-2) ) ||
                                                     ( ( i== 5) && (fila_diagonal <= i+1 && columna_diagonal >= j-1) )
                                                    ) &&
                                                   !stop_contar_adyacentes ){

                                                if( ( i != 0 ) && !casilla_anterior_comprobada_1 ){
                                                    fila_diagonal = i-1;
                                                    columna_diagonal = j+1;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                    casilla_anterior_comprobada_1 = true;
                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j-1;
                                                }
                                                if( casilla_anterior_comprobada_1 && ( ( i != 0 && i != 1 ) && !casilla_anterior_comprobada_2 ) ){
                                                    fila_diagonal = i-2;
                                                    columna_diagonal = j+2;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                    casilla_anterior_comprobada_2 = true;
                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j-1;
                                                }
                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                      estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) &&
                                                     !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal++;
                                                        columna_diagonal--;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal++;
                                                        columna_diagonal--;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }
                                        }
                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MAX++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"PIERDO en columna "<<j<< "de MAX DIAGONAL"<<endl;
                                            stop_asi_pierdo = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MAX++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }


                                        if( (i>=2 && i<=6) && !stop_asi_pierdo ){
                                            fila_diagonal = i-1; //incializamos la fila a una fila menos en caso de trabajar con filas de 3 a 6
                                            columna_diagonal = j-1; //volvemos a inicializar la columna
                                            casillas_libres = 0;
                                            aux_adyacente = 0;
                                            stop_contar_adyacentes = false;
                                            solo_cuenta_casillas_libres = false;
                                            casilla_anterior_comprobada_1 = false;
                                            casilla_anterior_comprobada_2 = false;
                                            while( ( ( ( i != 2 && i != 1 ) && fila_diagonal >= i-3 && columna_diagonal >= j-3 ) ||
                                                     ( ( i == 2 ) && fila_diagonal >= i-2 && columna_diagonal >= j-2 ) ||
                                                     ( ( i == 1  ) && fila_diagonal >= i-1 && columna_diagonal >= j-1 ) )
                                                     && !stop_contar_adyacentes ){
                                                  //  cout<<"whileee"<<endl;
                                                if( ( i != 6 ) && !casilla_anterior_comprobada_1 ){

                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j+1;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                     casilla_anterior_comprobada_1 = true;
                                                     fila_diagonal = i-1;
                                                     columna_diagonal = j-1;
                                                }
                                                if( casilla_anterior_comprobada_2 && ( ( i != 6 && i != 5 ) && !casilla_anterior_comprobada_2 ) ){

                                                    fila_diagonal = i+2;
                                                    columna_diagonal = j+2;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                     casilla_anterior_comprobada_1 = true;
                                                     fila_diagonal = i-1;
                                                     columna_diagonal = j-1;
                                                }
                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                        estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) &&
                                                        !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal--;
                                                        columna_diagonal--;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal--;
                                                        columna_diagonal--;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }
                                        }

                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MAX++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"PIERDO en columna "<<j<< "de MAX DIAGONAL"<<endl;
                                            stop_asi_pierdo = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MAX++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }
                                  } //if(j == 4)
                                  else if(j == 2){

                                        if(i>=0 && i<=5){

                                            fila_diagonal = i+1;
                                            columna_diagonal = j+1;
                                            while( ( ( ( i!= 4 && i != 5 ) && (fila_diagonal <= i+3 && columna_diagonal >= j+3) ) ||
                                                     ( ( i== 4) && (fila_diagonal <= i+2 && columna_diagonal >= j+2) ) ||
                                                     ( ( i== 5) && (fila_diagonal <= i+1 && columna_diagonal >= j+1) )
                                                    ) &&
                                                   !stop_contar_adyacentes ){

                                                if( ( i != 0 ) && !casilla_anterior_comprobada_1 ){
                                                    fila_diagonal = i-1;
                                                    columna_diagonal = j-1;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                    casilla_anterior_comprobada_1 = true;
                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j+1;
                                                }
                                                if( casilla_anterior_comprobada_1 && ( ( i != 0 && i != 1 ) && !casilla_anterior_comprobada_2 ) ){
                                                    fila_diagonal = i-2;
                                                    columna_diagonal = j-2;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                    casilla_anterior_comprobada_2 = true;
                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j+1;
                                                }
                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                      estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) &&
                                                     !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal++;
                                                        columna_diagonal++;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal++;
                                                        columna_diagonal++;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }
                                        }
                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MAX++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"PIERDO en columna "<<j<< "de MAX DIAGONAL"<<endl;
                                            stop_asi_pierdo = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MAX++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }


                                        if( (i>=2 && i<=6) && !stop_asi_pierdo ){
                                            fila_diagonal = i-1; //incializamos la fila a una fila menos en caso de trabajar con filas de 3 a 6
                                            columna_diagonal = j+1; //volvemos a inicializar la columna
                                            casillas_libres = 0;
                                            aux_adyacente = 0;
                                            stop_contar_adyacentes = false;
                                            solo_cuenta_casillas_libres = false;
                                            casilla_anterior_comprobada_1 = false;
                                            casilla_anterior_comprobada_2 = false;
                                            while( ( ( ( i != 2 && i != 1 ) && fila_diagonal >= i-3 && columna_diagonal <= j+3 ) ||
                                                     ( ( i == 2 ) && fila_diagonal >= i-2 && columna_diagonal <= j+2 ) ||
                                                     ( ( i == 1  ) && fila_diagonal >= i-1 && columna_diagonal <= j+1 ) )
                                                     && !stop_contar_adyacentes ){
                                                  //  cout<<"whileee"<<endl;
                                                if( ( i != 6 ) && !casilla_anterior_comprobada_1 ){

                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j-1;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                     casilla_anterior_comprobada_1 = true;
                                                     fila_diagonal = i-1;
                                                     columna_diagonal = j+1;
                                                }
                                                if( casilla_anterior_comprobada_2 && ( ( i != 6 && i != 5 ) && !casilla_anterior_comprobada_2 ) ){

                                                    fila_diagonal = i+2;
                                                    columna_diagonal = j-2;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                     casilla_anterior_comprobada_1 = true;
                                                     fila_diagonal = i-1;
                                                     columna_diagonal = j+1;
                                                }
                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                        estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) &&
                                                        !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal--;
                                                        columna_diagonal++;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal--;
                                                        columna_diagonal++;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }
                                        }

                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MAX++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"PIERDO en columna "<<j<< "de MAX DIAGONAL"<<endl;
                                            stop_asi_pierdo = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MAX++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MAX++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }
                                  } //if(j == 2)
                        }
                    }
                    else if( j == 3  && ( !stop_asi_gano && !stop_asi_pierdo ) ){ // j == 3
                     //   cout<<"Estoy en Jugador Max , 3 "<<endl;
                        aux_adyacente = 0;
                        for(col_aux = j+1 ; col_aux<7 &&  !stop_contar_adyacentes ; col_aux++){
                            if( ( ( estado.See_Casilla(i,col_aux) == jugador ) || ( estado.See_Casilla(i,col_aux) == jugador+3 ) ) &&
                                !solo_cuenta_casillas_libres){
                                aux_adyacente++;
                            }
                            else{
                                if(estado.See_Casilla(i,col_aux) == 0){
                                    solo_cuenta_casillas_libres=true;
                                    casillas_libres++;
                                }
                                else{
                                    stop_contar_adyacentes = true;
                                }
                            }
                        }
                        for(col_aux = j-1 ; col_aux>=0 &&  !stop_contar_adyacentes ; col_aux--){
                            if( ( ( estado.See_Casilla(i,col_aux) == jugador ) || ( estado.See_Casilla(i,col_aux) == jugador+3 ) ) &&
                                !solo_cuenta_casillas_libres ){
                                aux_adyacente++;
                            }
                            else{
                                if(estado.See_Casilla(i,col_aux) == 0){
                                    solo_cuenta_casillas_libres=true;
                                    casillas_libres++;
                                }
                                else{
                                    stop_contar_adyacentes = true;
                                }
                            }
                        }
                        if(  (aux_adyacente+1)<4 &&   (aux_adyacente+1)+casillas_libres>= 4  ){
                                //adyacentes_MAX += casillas_libres*10;
                            if(aux_adyacente+1 == 1){
                                    Caminos_a_partir_de_1_MAX++;
                            }
                            else if(aux_adyacente+1 == 2){
                                Caminos_a_partir_de_2_MAX++;
                            }
                            else if(aux_adyacente+1 == 3){
                                Caminos_a_partir_de_3_MAX++;
                            }
                        }

                        if(aux_adyacente+1 >= 4){
                            cout<<"PIERDO en columna "<<j<< "de MAX HORIZONTAL"<<endl;
                            stop_asi_pierdo = true;
                         //   cout<<"Pierdo"<<endl;
                        }
                        else if(aux_adyacente != 0){
                         //  adyacentes_MAX += (aux_adyacente+1)*10;
                            if(aux_adyacente+1 == 2){
                                adyacente_de_2_MAX++;
                            }
                            else if(aux_adyacente+1 == 3){
                                adyacente_de_3_MAX++;
                            }
                           aux_adyacente = 0;
                        }
                        if(estado.Get_Ocupacion_Columna(j) != 0){
                            i = estado.Get_Ocupacion_Columna(j) - 1 ;
                        }

                      if(!stop_asi_pierdo){
                            aux_adyacente = 0;
                            casillas_libres = 0;
                            if( estado.Get_Ocupacion_Columna(j) > 0)
                                i = estado.Get_Ocupacion_Columna(j) - 1 ;
                            solo_cuenta_casillas_libres = false;
                            stop_contar_adyacentes = false;
                            int fila_diagonal;
                            int columna_diagonal ;
                            bool casilla_anterior_comprobada_1 = false;
                            bool casilla_anterior_comprobada_2 = false;
                            bool casilla_anterior_comprobada_3 = false;

                                        if(i>=0 && i<=5){ /** Primera Comparaci�n **/


                                                if( ( i != 0 ) && !casilla_anterior_comprobada_1 ){
                                                    fila_diagonal = i-1;
                                                    columna_diagonal = j-1;
                                                    if( !solo_cuenta_casillas_libres && !stop_contar_adyacentes &&
                                                        ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                            solo_cuenta_casillas_libres = true;
                                                     }
                                                    else{
                                                            stop_contar_adyacentes = true;
                                                        }
                                                    casilla_anterior_comprobada_1 = true;

                                                }
                                                if( casilla_anterior_comprobada_1 && ( ( i != 0 && i != 1 ) && !casilla_anterior_comprobada_2 ) ){
                                                    fila_diagonal = i-2;
                                                    columna_diagonal = j-2;
                                                    if(!solo_cuenta_casillas_libres && !stop_contar_adyacentes &&
                                                        ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                            solo_cuenta_casillas_libres = true;
                                                     }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                    casilla_anterior_comprobada_2 = true;

                                                }
                                                if( casilla_anterior_comprobada_2 && ( ( i != 0 && i !=1 && i != 2 ) && !casilla_anterior_comprobada_3 ) ){
                                                    fila_diagonal = i-3;
                                                    columna_diagonal = j-3;
                                                    if(!solo_cuenta_casillas_libres && !stop_contar_adyacentes &&
                                                        ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                            solo_cuenta_casillas_libres = true;
                                                     }
                                                     else{
                                                        stop_contar_adyacentes = true;
                                                     }
                                                    casilla_anterior_comprobada_3 = true;
                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j+1;
                                                }
                                            stop_contar_adyacentes = false;
                                            solo_cuenta_casillas_libres = false;
                                            fila_diagonal = i+1;
                                            columna_diagonal = j+1;
                                            while( ( ( ( i!= 4 && i != 5 ) && (fila_diagonal <= i+3 && columna_diagonal <= j+3) ) ||
                                                     ( ( i== 4) && (fila_diagonal <= i+2 && columna_diagonal <= j+2) ) ||
                                                     ( ( i== 5) && (fila_diagonal <= i+1 && columna_diagonal <= j+1) )
                                                    ) &&
                                                   !stop_contar_adyacentes ){

                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                      estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) &&
                                                     !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal++;
                                                        columna_diagonal++;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal++;
                                                        columna_diagonal++;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }

                                            if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                    //adyacentes_MAX += casillas_libres*10;
                                                if(aux_adyacente+1 == 1){
                                                    Caminos_a_partir_de_1_MAX++;
                                                }
                                                else if(aux_adyacente+1 == 2){
                                                    Caminos_a_partir_de_2_MAX++;
                                                }
                                                else if(aux_adyacente+1 == 3){
                                                    Caminos_a_partir_de_3_MAX++;
                                                }
                                            }
                                            if( (aux_adyacente+1) >= 4 ){
                                                cout<<"PIERDO en columna "<<j<< "de MAX DIAGONAL"<<endl;
                                                stop_asi_pierdo = true;
                                               // cout<<"Pierdo"<<endl;
                                            }
                                            else if( aux_adyacente != 0){
                                               // adyacentes_MAX += (aux_adyacente+1)*10;
                                               // aux_adyacente = 0;
                                                if(aux_adyacente+1 == 2){
                                                    adyacente_de_2_MAX++;
                                                }
                                                else if(aux_adyacente+1 == 3){
                                                    adyacente_de_3_MAX++;
                                                }
                                              //  adyacentes_MAX += (aux_adyacente+1)*10;
                                                aux_adyacente = 0;
                                            }
                                             aux_adyacente = 0;
                                            casillas_libres = 0;
                                            if( estado.Get_Ocupacion_Columna(j) > 0)
                                                i = estado.Get_Ocupacion_Columna(j) - 1 ;
                                            solo_cuenta_casillas_libres = false;
                                            stop_contar_adyacentes = false;
                                            fila_diagonal;
                                            columna_diagonal ;
                                            casilla_anterior_comprobada_1 = false;
                                            casilla_anterior_comprobada_2 = false;
                                            casilla_anterior_comprobada_3 = false;


                                                  if( ( i != 0 ) && !casilla_anterior_comprobada_1 ){
                                                        fila_diagonal = i-1;
                                                        columna_diagonal = j+1;
                                                        if( !stop_contar_adyacentes && !solo_cuenta_casillas_libres && ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                              estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) ){
                                                                aux_adyacente++;
                                                         }
                                                         else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                                casillas_libres++;
                                                                solo_cuenta_casillas_libres = true;
                                                         }
                                                         else{
                                                            stop_contar_adyacentes = true;
                                                         }
                                                        casilla_anterior_comprobada_1 = true;

                                                    }
                                                    if(  casilla_anterior_comprobada_1 && ( ( i != 0 && i != 1 ) && !casilla_anterior_comprobada_2 ) ){
                                                        fila_diagonal = i-2;
                                                        columna_diagonal = j+2;
                                                        if(!solo_cuenta_casillas_libres && !stop_contar_adyacentes &&
                                                            ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                              estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) ){
                                                                aux_adyacente++;
                                                         }
                                                         else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                                casillas_libres++;
                                                                solo_cuenta_casillas_libres = true;
                                                         }
                                                        else{
                                                            stop_contar_adyacentes = true;
                                                        }
                                                        casilla_anterior_comprobada_2 = true;

                                                    }
                                                    if( casilla_anterior_comprobada_2 && ( ( i != 0 && i !=1 && i != 2 ) && !casilla_anterior_comprobada_3 ) ){
                                                        fila_diagonal = i-3;
                                                        columna_diagonal = j+3;
                                                        if(!solo_cuenta_casillas_libres && !stop_contar_adyacentes &&
                                                            ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                              estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) ){
                                                                aux_adyacente++;
                                                         }
                                                         else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                                casillas_libres++;
                                                                solo_cuenta_casillas_libres = true;
                                                         }
                                                        else{
                                                            stop_contar_adyacentes = true;
                                                        }
                                                        casilla_anterior_comprobada_3 = true;

                                                    }
                                                    solo_cuenta_casillas_libres = false;
                                                    stop_contar_adyacentes = false;

                                            fila_diagonal = i+1;
                                            columna_diagonal = j-1;
                                            while( ( ( ( i!= 4 && i != 5 ) && (fila_diagonal <= i+3 && columna_diagonal >= j-3) ) ||
                                                         ( ( i== 4) && (fila_diagonal <= i+2 && columna_diagonal >= j-2) ) ||
                                                         ( ( i== 5) && (fila_diagonal <= i+1 && columna_diagonal >= j-1) )
                                                        ) &&
                                                       !stop_contar_adyacentes ){


                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) &&
                                                         !solo_cuenta_casillas_libres)
                                                    {
                                                            aux_adyacente++;
                                                            fila_diagonal++;
                                                            columna_diagonal--;

                                                    }
                                                    else{
                                                        if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            solo_cuenta_casillas_libres = true;
                                                            casillas_libres++;

                                                            fila_diagonal++;
                                                            columna_diagonal--;
                                                        }
                                                        else{
                                                            stop_contar_adyacentes = true;
                                                        }
                                                    }

                                                }

                                            if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                    //adyacentes_MAX += casillas_libres*10;
                                                if(aux_adyacente+1 == 1){
                                                    Caminos_a_partir_de_1_MAX++;
                                                }
                                                else if(aux_adyacente+1 == 2){
                                                    Caminos_a_partir_de_2_MAX++;
                                                }
                                                else if(aux_adyacente+1 == 3){
                                                    Caminos_a_partir_de_3_MAX++;
                                                }
                                            }
                                            if( (aux_adyacente+1) >= 4 ){
                                                cout<<"PIERDO en columna "<<j<< "de MAX DIAGONAL"<<endl;
                                                stop_asi_pierdo = true;
                                               // cout<<"Pierdo"<<endl;
                                            }
                                            else if( aux_adyacente != 0){
                                               // adyacentes_MAX += (aux_adyacente+1)*10;
                                               // aux_adyacente = 0;
                                                if(aux_adyacente+1 == 2){
                                                    adyacente_de_2_MAX++;
                                                }
                                                else if(aux_adyacente+1 == 3){
                                                    adyacente_de_3_MAX++;
                                                }
                                              //  adyacentes_MAX += (aux_adyacente+1)*10;
                                                aux_adyacente = 0;
                                            }
                                    }
                                    else if( i == 6){
                                             fila_diagonal = i-1;
                                            columna_diagonal = j+1;
                                            while( (fila_diagonal >= i-3 && columna_diagonal <= j+3) && !stop_contar_adyacentes ){
                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                      estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) &&
                                                     !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal--;
                                                        columna_diagonal++;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal--;
                                                        columna_diagonal++;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }

                                            if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                    //adyacentes_MAX += casillas_libres*10;
                                                if(aux_adyacente+1 == 1){
                                                    Caminos_a_partir_de_1_MAX++;
                                                }
                                                else if(aux_adyacente+1 == 2){
                                                    Caminos_a_partir_de_2_MAX++;
                                                }
                                                else if(aux_adyacente+1 == 3){
                                                    Caminos_a_partir_de_3_MAX++;
                                                }
                                            }
                                            if( (aux_adyacente+1) >= 4 ){
                                                cout<<"PIERDO en columna "<<j<< "de MAX DIAGONAL"<<endl;
                                                stop_asi_pierdo = true;
                                               // cout<<"Pierdo"<<endl;
                                            }
                                            else if( aux_adyacente != 0){
                                               // adyacentes_MAX += (aux_adyacente+1)*10;
                                               // aux_adyacente = 0;
                                                if(aux_adyacente+1 == 2){
                                                    adyacente_de_2_MAX++;
                                                }
                                                else if(aux_adyacente+1 == 3){
                                                    adyacente_de_3_MAX++;
                                                }
                                              //  adyacentes_MAX += (aux_adyacente+1)*10;
                                                aux_adyacente = 0;
                                            }
                                            fila_diagonal = i-1;
                                            columna_diagonal = j-1;
                                            while( (fila_diagonal >= i-3 && columna_diagonal >= j-3) && !stop_contar_adyacentes ){

                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador ||
                                                      estado.See_Casilla(fila_diagonal,columna_diagonal) == jugador+3 ) &&
                                                     !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal--;
                                                        columna_diagonal--;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal--;
                                                        columna_diagonal--;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }

                                            if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                    //adyacentes_MAX += casillas_libres*10;
                                                if(aux_adyacente+1 == 1){
                                                    Caminos_a_partir_de_1_MAX++;
                                                }
                                                else if(aux_adyacente+1 == 2){
                                                    Caminos_a_partir_de_2_MAX++;
                                                }
                                                else if(aux_adyacente+1 == 3){
                                                    Caminos_a_partir_de_3_MAX++;
                                                }
                                            }
                                            if( (aux_adyacente+1) >= 4 ){
                                                cout<<"PIERDO en columna "<<j<< "de MAX DIAGONAL"<<endl;
                                                stop_asi_pierdo = true;
                                               // cout<<"Pierdo"<<endl;
                                            }
                                            else if( aux_adyacente != 0){
                                               // adyacentes_MAX += (aux_adyacente+1)*10;
                                               // aux_adyacente = 0;
                                                if(aux_adyacente+1 == 2){
                                                    adyacente_de_2_MAX++;
                                                }
                                                else if(aux_adyacente+1 == 3){
                                                    adyacente_de_3_MAX++;
                                                }
                                              //  adyacentes_MAX += (aux_adyacente+1)*10;
                                                aux_adyacente = 0;
                                            }

                                    }
                            }

                    }

                   if(estado.Get_Ocupacion_Columna(j) != 0){
                        i = estado.Get_Ocupacion_Columna(j) - 1 ;
                   }
                   aux_adyacente = 0;

              }


              else if(  estado.See_Casilla(i,j) == Jugador_Min || estado.See_Casilla(i,j) == Jugador_Min+3 ){
                /*
                    contar los adyacentes en vertical
                */
                 //   cout<<"Estoy en Jugador Max , Vertical"<<endl;
                    aux_adyacente=0;
                    stop_contar_adyacentes = false;
                    if(estado.Get_Ocupacion_Columna(j) >1 ){
                        for(i = estado.Get_Ocupacion_Columna(j)-1 ; i>=0  && !stop_contar_adyacentes ; i--){
                            if( estado.See_Casilla(i,j) == Jugador_Min || estado.See_Casilla(i,j) == Jugador_Min+3 ){ //
                                aux_adyacente++;
                            }
                            else{
                                stop_contar_adyacentes = true;
                            //    cout<<aux_adyacente<<endl;
                            }
                        }
                    //    cout<<"Columna : "<<j<<" adyacentes : "<<aux_adyacente<<endl;
                        //contar las posibilidades de comletar 4 casillas seguidas
                      //  i = estado.Get_Ocupacion_Columna(j) - 1;


                    }
                    if( ( (aux_adyacente) + ( 7 - estado.Get_Ocupacion_Columna(j) ) ) >= 4  ){
                            //adyacentes_MAX += (4-aux_adyacente)*20;
                        if( (aux_adyacente) == 1){
                            Caminos_a_partir_de_1_MIN++;
                        }
                        else if( (aux_adyacente) == 2){
                            Caminos_a_partir_de_2_MIN++;
                        }
                        else if( (aux_adyacente) == 3){
                            Caminos_a_partir_de_3_MIN++;
                        }
                    }

                    if( (aux_adyacente) >= 4){
                            cout<<"Gano en columna "<<j<< "de MIN VERTICAL"<<endl;

                            stop_asi_gano = true;
                          //  cout<<"Pierdo"<<endl;
                    }
                    else if( aux_adyacente != 0){
                        if( (aux_adyacente) == 2){
                            adyacente_de_2_MIN++;
                        }
                        else if( (aux_adyacente) == 3){
                            adyacente_de_3_MIN++;
                        }
                      //  adyacentes_MAX += (aux_adyacente+1)*10;
                        aux_adyacente = 0;
                    }

                    stop_contar_adyacentes=false; //asignar de nuevo a false "se va a utilizar de nuevo"
                    col_aux = j;
                    if(estado.Get_Ocupacion_Columna(j) != 0){
                        i = estado.Get_Ocupacion_Columna(j) - 1 ;
                    }
                            //contar los adyacentes en horizontal

                            //Caso de que la casilla comprobada es un extremo " 6 � 0 "

                    if(j == 0 || j == 6 && ( !stop_asi_gano && !stop_asi_pierdo ) ){ //caso de extremos , pues se ahorra una comparaci�n
                        aux_adyacente = 0;
                        if(j == 0){
                        //    cout<<"Estoy en Jugador Max , 0 "<<endl;
                            for(col_aux = j+1 ; col_aux < 4 && !stop_contar_adyacentes ; col_aux++){
                                if( ( estado.See_Casilla(i,col_aux) == Jugador_Min || estado.See_Casilla(i,col_aux) == Jugador_Min+3 ) &&
                                     !solo_cuenta_casillas_libres
                                   ){ //
                                        aux_adyacente++;
                                }
                                else{
                                    if(estado.See_Casilla(i,col_aux) == 0){
                                        solo_cuenta_casillas_libres = true;
                                        casillas_libres++;
                                    }
                                    else{
                                        stop_contar_adyacentes = true;
                                    }

                                }
                            }

                        }
                        else{
                        //    cout<<"Estoy en Jugador Max , 6 "<<endl;
                            for(int col_aux = j-1 ; col_aux > 2 && !stop_contar_adyacentes ; col_aux--){
                                if( ( estado.See_Casilla(i,col_aux) == Jugador_Min || estado.See_Casilla(i,col_aux) == Jugador_Min+3 ) &&
                                     !solo_cuenta_casillas_libres ){ //
                                    aux_adyacente++;
                                }
                                else{
                                    if(estado.See_Casilla(i,col_aux) == 0){
                                        solo_cuenta_casillas_libres = true;
                                        casillas_libres++;
                                    }
                                    else{
                                        stop_contar_adyacentes = true;
                                    }

                                }
                            }

                        }
                        if(  (aux_adyacente+1)<4 &&   (aux_adyacente+1)+casillas_libres>= 4  ){
                                //adyacentes_MAX += casillas_libres*10;
                            if(aux_adyacente+1 == 1){
                                Caminos_a_partir_de_1_MIN++;
                            }
                            else if(aux_adyacente+1 == 2){
                                Caminos_a_partir_de_2_MIN++;
                            }
                            else if(aux_adyacente+1 == 3){
                                Caminos_a_partir_de_3_MIN++;
                            }
                        }

                        if( (aux_adyacente+1) >= 4 ){
                            cout<<" Gano en columna "<<j<< "de MIN HORIZONTAL "<<endl;
                            stop_asi_gano = true;
                           // cout<<"Pierdo"<<endl;
                        }
                        else if( aux_adyacente != 0){
                           // adyacentes_MAX += (aux_adyacente+1)*10;
                           // aux_adyacente = 0;
                            if(aux_adyacente+1 == 2){
                                adyacente_de_2_MIN++;
                            }
                            else if(aux_adyacente+1 == 3){
                                adyacente_de_3_MIN++;
                            }
                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                            aux_adyacente = 0;
                        }
                        if(estado.Get_Ocupacion_Columna(j) != 0){
                            i = estado.Get_Ocupacion_Columna(j) - 1 ;
                        }
                        stop_contar_adyacentes = false;
                        casillas_libres = 0;
                        aux_adyacente = 0;

                        /***  Calcular las secuencias diagonales Para j = 6 , j = 0  ***/

                        if(!stop_asi_gano){
                            aux_adyacente = 0;
                            casillas_libres = 0;
                            if( estado.Get_Ocupacion_Columna(j) > 0)
                                i = estado.Get_Ocupacion_Columna(j) - 1 ;
                            solo_cuenta_casillas_libres = false;
                            stop_contar_adyacentes = false;
                            int fila_diagonal;
                            int columna_diagonal ;

                            if(j == 0 && estado.Get_Ocupacion_Columna(j) > 0){

                                        if(i>=0 && i<=3){
                                            fila_diagonal = i+1;
                                            columna_diagonal = j+1;
                                            while( (fila_diagonal <= i+3 && columna_diagonal <= j+3) && !stop_contar_adyacentes ){

                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                      estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) &&
                                                     !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal++;
                                                        columna_diagonal++;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal++;
                                                        columna_diagonal++;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }
                                        }
                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MIN++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"Gano en columna "<<j<< "de MIN DIAGONAL"<<endl;
                                            stop_asi_gano = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MIN++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }


                                        if( (i>=3 && i<=6) && !stop_asi_gano ){
                                         //   cout<<"estoy dentro"<<endl;
                                            fila_diagonal = i-1; //incializamos la fila a una fila menos en caso de trabajar con filas de 3 a 6
                                          //  cout<<fila_diagonal<<endl;
                                            columna_diagonal = j+1; //volvemos a inicializar la columna
                                            casillas_libres = 0;
                                            aux_adyacente = 0;
                                            stop_contar_adyacentes = false;
                                            solo_cuenta_casillas_libres = false;
                                            while( (fila_diagonal <= i+3 && columna_diagonal <= j+3) && !stop_contar_adyacentes ){
                                                  //  cout<<"whileee"<<endl;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) &&
                                                         !solo_cuenta_casillas_libres)
                                                    {
                                                            aux_adyacente++;
                                                            fila_diagonal--;
                                                            columna_diagonal++;

                                                    }
                                                    else{
                                                        if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            solo_cuenta_casillas_libres = true;
                                                            casillas_libres++;

                                                            fila_diagonal--;
                                                            columna_diagonal++;
                                                        }
                                                        else{
                                                            stop_contar_adyacentes = true;
                                                        }
                                                    }

                                            }
                                        }

                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MIN++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"Gano en columna "<<j<< "de MIN DIAGONAL"<<endl;
                                            stop_asi_gano = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MIN++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }
                                  } //if(j == 0)
                                  else if(j == 6){
                                        if(i>=0 && i<=3){
                                            fila_diagonal = i+1;
                                            columna_diagonal = j-1;
                                            while( (fila_diagonal <= i+3 && columna_diagonal >= j-3) && !stop_contar_adyacentes ){

                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                      estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) &&
                                                     !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal++;
                                                        columna_diagonal--;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal++;
                                                        columna_diagonal--;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }
                                        }
                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MIN++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"Gano en columna "<<j<< "de MIN DIAGONAL"<<endl;
                                            stop_asi_gano = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MIN++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }


                                        if( (i>=3 && i<=6) && !stop_asi_gano ){
                                         //   cout<<"estoy dentro"<<endl;
                                            fila_diagonal = i-1; //incializamos la fila a una fila menos en caso de trabajar con filas de 3 a 6
                                          //  cout<<fila_diagonal<<endl;
                                            columna_diagonal = j-1; //volvemos a inicializar la columna
                                            casillas_libres = 0;
                                            aux_adyacente = 0;
                                            stop_contar_adyacentes = false;
                                            solo_cuenta_casillas_libres = false;
                                            while( (fila_diagonal >= i-3 && columna_diagonal >= j-3) && !stop_contar_adyacentes ){
                                                  //  cout<<"whileee"<<endl;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) &&
                                                         !solo_cuenta_casillas_libres)
                                                    {
                                                            aux_adyacente++;
                                                            fila_diagonal--;
                                                            columna_diagonal--;

                                                    }
                                                    else{
                                                        if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            solo_cuenta_casillas_libres = true;
                                                            casillas_libres++;

                                                            fila_diagonal--;
                                                            columna_diagonal++;
                                                        }
                                                        else{
                                                            stop_contar_adyacentes = true;
                                                        }
                                                    }

                                            }
                                        }

                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MIN++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"Gano en columna "<<j<< "de MAX DIAGONAL"<<endl;
                                            stop_asi_gano = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MIN++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }

                                  }
                            } //if(!stop_asi_pierdo) //calcular los diagonales


                    }


                            //caso de un casilla adyavente a uno de los extremos
                    else if(j == 1 || j == 5 && ( !stop_asi_gano && !stop_asi_pierdo ) ){
                        aux_adyacente = 0;
                        if(j == 1){
                        //    cout<<"Estoy en Jugador Max , 1 "<<endl;
                            if( estado.See_Casilla(i,j-1) == Jugador_Min || estado.See_Casilla(i,j-1) == Jugador_Min+3 ){
                                aux_adyacente++;
                            }
                            for(col_aux = j+1 ; col_aux<5 && !stop_contar_adyacentes ; col_aux++){

                                if( ( estado.See_Casilla(i,col_aux) == Jugador_Min || estado.See_Casilla(i,col_aux) == Jugador_Min+3 ) &&
                                    !solo_cuenta_casillas_libres){
                                    aux_adyacente++;
                                }
                                else{
                                    if(estado.See_Casilla(i,col_aux) == 0){
                                        solo_cuenta_casillas_libres = true;
                                        casillas_libres++;
                                    }
                                    else{
                                        stop_contar_adyacentes = true;
                                    }

                                }
                            }

                        }
                        else{
                       //      cout<<"Estoy en Jugador Max , 5 "<<endl;
                            if( estado.See_Casilla(i,j+1) == Jugador_Min || estado.See_Casilla(i,j+1) == Jugador_Min+3 ){
                                aux_adyacente++;
                            }
                            for(col_aux = j-1 ; col_aux>1 &&  !stop_contar_adyacentes ; col_aux--){
                                if( ( estado.See_Casilla(i,col_aux) == Jugador_Min || estado.See_Casilla(i,col_aux) == Jugador_Min+3 ) &&
                                    !solo_cuenta_casillas_libres){
                                    aux_adyacente++;
                                }
                                else{
                                    if(estado.See_Casilla(i,col_aux) == 0){
                                        solo_cuenta_casillas_libres = true;
                                        casillas_libres++;
                                    }
                                    else{
                                        stop_contar_adyacentes = true;
                                    }

                                }
                            }

                        }
                        if(  (aux_adyacente+1)<4 &&   (aux_adyacente+1)+casillas_libres>= 4  ){
                                //adyacentes_MAX += casillas_libres*10;
                            if(aux_adyacente+1 == 1){
                                Caminos_a_partir_de_1_MIN++;
                            }
                            else if(aux_adyacente+1 == 2){
                                Caminos_a_partir_de_2_MIN++;
                            }
                            else if(aux_adyacente+1 == 3){
                                Caminos_a_partir_de_3_MIN++;
                            }
                        }

                        if(aux_adyacente+1 >= 4){
                            cout<<"Gano en columna "<<j<< "de MIN HORIZONTAL"<<endl;
                            stop_asi_gano = true;
                         //   cout<<"Pierdo"<<endl;
                        }
                        else if(aux_adyacente != 0){
                            //adyacentes_MAX += (aux_adyacente+1)*10;
                            if(aux_adyacente+1 == 2){
                                adyacente_de_2_MIN++;
                            }
                            else if(aux_adyacente+1 == 3){
                                adyacente_de_3_MIN++;
                            }
                            aux_adyacente = 0;
                        }
                        if(estado.Get_Ocupacion_Columna(j) != 0){
                            i = estado.Get_Ocupacion_Columna(j) - 1 ;
                        }
                        /***  Calcular las secuencias diagonales Para j = 5 , j = 1  ***/

                        if(!stop_asi_pierdo){
                            aux_adyacente = 0;
                            casillas_libres = 0;
                            if( estado.Get_Ocupacion_Columna(j) > 0)
                                i = estado.Get_Ocupacion_Columna(j) - 1 ;
                            solo_cuenta_casillas_libres = false;
                            stop_contar_adyacentes = false;
                            int fila_diagonal;
                            int columna_diagonal ;
                            bool casilla_anterior_comprobada = false;

                            if(j == 5){

                                        if(i>=0 && i<=4){

                                            fila_diagonal = i+1;
                                            columna_diagonal = j-1;
                                            while( ( ( ( i!= 4) && (fila_diagonal <= i+3 && columna_diagonal >= j-3) ) ||
                                                     ( ( i== 4) && (fila_diagonal <= i+2 && columna_diagonal >= j-2) )
                                                    ) &&
                                                   !stop_contar_adyacentes ){

                                                if( ( i != 0 ) && !casilla_anterior_comprobada ){
                                                    fila_diagonal = i-1;
                                                    columna_diagonal = j+1;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                    casilla_anterior_comprobada = true;
                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j-1;
                                                }
                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                      estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) &&
                                                     !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal++;
                                                        columna_diagonal--;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal++;
                                                        columna_diagonal--;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }
                                        }
                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MIN++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"Gano en columna "<<j<< "de MIN DIAGONAL"<<endl;
                                            stop_asi_gano = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MIN++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }


                                        if( (i>=3 && i<=6) && !stop_asi_pierdo ){
                                            fila_diagonal = i-1; //incializamos la fila a una fila menos en caso de trabajar con filas de 3 a 6
                                            columna_diagonal = j-1; //volvemos a inicializar la columna
                                            casillas_libres = 0;
                                            aux_adyacente = 0;
                                            stop_contar_adyacentes = false;
                                            solo_cuenta_casillas_libres = false;
                                            casilla_anterior_comprobada = false;
                                            while( (fila_diagonal >= i-3 && columna_diagonal >= j-3) && !stop_contar_adyacentes ){
                                                  //  cout<<"whileee"<<endl;
                                                if( ( i != 6 ) && !casilla_anterior_comprobada ){

                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j+1;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                     casilla_anterior_comprobada = true;
                                                     fila_diagonal = i-1;
                                                     columna_diagonal = j-1;
                                                }
                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                        estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) &&
                                                        !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal--;
                                                        columna_diagonal--;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal--;
                                                        columna_diagonal--;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }
                                        }

                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MIN++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"Gano en columna "<<j<< "de MIN DIAGONAL"<<endl;
                                            stop_asi_gano = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MIN++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }
                                  } //if(j == 0)
                                 else if(j == 1){
                                       if(i>=0 && i<=4){

                                            fila_diagonal = i+1;
                                            columna_diagonal = j+1;
                                            while( ( ( ( i!= 4) && (fila_diagonal <= i+3 && columna_diagonal <= j+3) ) ||
                                                     ( ( i == 4) && (fila_diagonal <= i+2 && columna_diagonal <= j+2) )
                                                    ) &&
                                                   !stop_contar_adyacentes ){

                                                if( ( i != 0 ) && !casilla_anterior_comprobada ){
                                                    fila_diagonal = i-1;
                                                    columna_diagonal = j-1;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                    casilla_anterior_comprobada = true;
                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j+1;
                                                }
                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                      estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) &&
                                                     !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal++;
                                                        columna_diagonal++;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal++;
                                                        columna_diagonal++;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }
                                        }
                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MIN++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"Gano en columna "<<j<< "de MIN DIAGONAL"<<endl;
                                            stop_asi_gano = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MIN++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }


                                        if( (i>=2 && i<=6) && !stop_asi_gano ){
                                            fila_diagonal = i-1; //incializamos la fila a una fila menos en caso de trabajar con filas de 3 a 6
                                            columna_diagonal = j+1; //volvemos a inicializar la columna
                                            casillas_libres = 0;
                                            aux_adyacente = 0;
                                            stop_contar_adyacentes = false;
                                            solo_cuenta_casillas_libres = false;
                                            casilla_anterior_comprobada = false;
                                            while( ( ( ( i != 2 ) && fila_diagonal >= i-3 && columna_diagonal <= j+3 ) ||
                                                     ( ( i == 2 ) && fila_diagonal >= i-2 && columna_diagonal <= j+2 )
                                                    )
                                                     && !stop_contar_adyacentes ){
                                                  //  cout<<"whileee"<<endl;
                                                if( ( i != 6 ) && !casilla_anterior_comprobada ){

                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j-1;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                     casilla_anterior_comprobada = true;
                                                     fila_diagonal = i-1;
                                                     columna_diagonal = j+1;
                                                }
                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                        estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) &&
                                                        !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal--;
                                                        columna_diagonal++;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal--;
                                                        columna_diagonal++;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }
                                        }

                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MIN++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"Gano en columna "<<j<< "de MIN DIAGONAL"<<endl;
                                            stop_asi_gano = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MIN++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }
                                  } //if(j == 0)
                            }
                    }
                    else if( (j == 2 || j == 4 ) && ( !stop_asi_pierdo && !stop_asi_gano )){
                        if(j == 2){
                            aux_adyacente = 0;
                        //     cout<<"Estoy en Jugador Max , 2 "<<endl;
                            if( estado.See_Casilla(i,j-1) == Jugador_Min || estado.See_Casilla(i,j-1) == Jugador_Min+3 ){
                                aux_adyacente++;
                            }
                            if( ( estado.See_Casilla(i,j-1) == Jugador_Min || estado.See_Casilla(i,j-1) == Jugador_Min+3 ) &&
                                ( estado.See_Casilla(i,j-2) == Jugador_Min || estado.See_Casilla(i,j-2) == Jugador_Min+3 ) ){
                                aux_adyacente++;
                            }
                            for(col_aux = j+1 ; col_aux<6 &&  !stop_contar_adyacentes ; col_aux++){
                                if( ( estado.See_Casilla(i,col_aux) == Jugador_Min || estado.See_Casilla(i,col_aux) == Jugador_Min+3 ) &&
                                    !solo_cuenta_casillas_libres ){
                                    aux_adyacente++;
                                }
                                else{
                                    if(estado.See_Casilla(i,col_aux) == 0){
                                        solo_cuenta_casillas_libres=true;
                                        casillas_libres++;
                                    }
                                    else{
                                        stop_contar_adyacentes = true;
                                    }

                                }
                            }

                        }
                        else{
                            aux_adyacente = 0;
                        //     cout<<"Estoy en Jugador Max , 4 "<<endl;
                            if( estado.See_Casilla(i,j+1) == Jugador_Min || estado.See_Casilla(i,j+1) == Jugador_Min+3 ){
                                aux_adyacente++;
                            }
                            if( ( estado.See_Casilla(i,j+1) == Jugador_Min || estado.See_Casilla(i,j+1) == Jugador_Min+3 ) &&
                                ( estado.See_Casilla(i,j+2) == Jugador_Min || estado.See_Casilla(i,j+2) == Jugador_Min+3 ) ){
                                aux_adyacente++;
                            }
                            for(col_aux = j-1 ; col_aux>0 &&  !stop_contar_adyacentes ; col_aux--){
                                if( ( estado.See_Casilla(i,col_aux) == Jugador_Min || estado.See_Casilla(i,col_aux) == Jugador_Min+3 ) &&
                                   !solo_cuenta_casillas_libres ){
                                    aux_adyacente++;
                                }
                                else{
                                    if(estado.See_Casilla(i,col_aux) == 0){
                                        solo_cuenta_casillas_libres=true;
                                        casillas_libres++;
                                    }
                                    else{
                                        stop_contar_adyacentes = true;
                                    }
                                }
                            }

                        }
                        if(  (aux_adyacente+1)<4 &&   (aux_adyacente+1)+casillas_libres>= 4  ){
                                //adyacentes_MAX += casillas_libres*10;
                            if(aux_adyacente+1 == 1){
                                Caminos_a_partir_de_1_MIN++;
                            }
                            else if(aux_adyacente+1 == 2){
                                Caminos_a_partir_de_2_MIN++;
                            }
                            else if(aux_adyacente+1 == 3){
                                Caminos_a_partir_de_3_MIN++;
                            }
                        }

                        if(aux_adyacente+1 >= 4){
                            cout<<"Gano en columna "<<j<< "de MIN HORIZONTAL"<<endl;
                            stop_asi_gano = true;
                          //  cout<<"Pierdo"<<endl;
                        }
                        else if(aux_adyacente != 0){
                            if(aux_adyacente+1 == 2){
                                adyacente_de_2_MIN++;
                            }
                            else if(aux_adyacente+1 == 3){
                                adyacente_de_3_MIN++;
                            }
                            aux_adyacente = 0;
                        }
                        if(estado.Get_Ocupacion_Columna(j) != 0){
                            i = estado.Get_Ocupacion_Columna(j) - 1 ;
                        }
                      if(!stop_asi_gano){
                            aux_adyacente = 0;
                            casillas_libres = 0;
                            if( estado.Get_Ocupacion_Columna(j) > 0)
                                i = estado.Get_Ocupacion_Columna(j) - 1 ;
                            solo_cuenta_casillas_libres = false;
                            stop_contar_adyacentes = false;
                            int fila_diagonal;
                            int columna_diagonal ;
                            bool casilla_anterior_comprobada_1 = false;
                            bool casilla_anterior_comprobada_2 = false;
                            if(j == 4){

                                        if(i>=0 && i<=5){

                                            fila_diagonal = i+1;
                                            columna_diagonal = j-1;
                                            while( ( ( ( i!= 4 && i != 5 ) && (fila_diagonal <= i+3 && columna_diagonal >= j-3) ) ||
                                                     ( ( i== 4) && (fila_diagonal <= i+2 && columna_diagonal >= j-2) ) ||
                                                     ( ( i== 5) && (fila_diagonal <= i+1 && columna_diagonal >= j-1) )
                                                    ) &&
                                                   !stop_contar_adyacentes ){

                                                if( ( i != 0 ) && !casilla_anterior_comprobada_1 ){
                                                    fila_diagonal = i-1;
                                                    columna_diagonal = j+1;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                    casilla_anterior_comprobada_1 = true;
                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j-1;
                                                }
                                                if( casilla_anterior_comprobada_1 && ( ( i != 0 && i != 1 ) && !casilla_anterior_comprobada_2 ) ){
                                                    fila_diagonal = i-2;
                                                    columna_diagonal = j+2;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                    casilla_anterior_comprobada_2 = true;
                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j-1;
                                                }
                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                      estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) &&
                                                     !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal++;
                                                        columna_diagonal--;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal++;
                                                        columna_diagonal--;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }
                                        }
                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MIN++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"GANO en columna "<<j<< "de MIN DIAGONAL"<<endl;
                                            stop_asi_gano = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MIN++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }


                                        if( (i>=2 && i<=6) && !stop_asi_gano ){
                                            fila_diagonal = i-1; //incializamos la fila a una fila menos en caso de trabajar con filas de 3 a 6
                                            columna_diagonal = j-1; //volvemos a inicializar la columna
                                            casillas_libres = 0;
                                            aux_adyacente = 0;
                                            stop_contar_adyacentes = false;
                                            solo_cuenta_casillas_libres = false;
                                            casilla_anterior_comprobada_1 = false;
                                            casilla_anterior_comprobada_2 = false;
                                            while( ( ( ( i != 2 && i != 1 ) && fila_diagonal >= i-3 && columna_diagonal >= j-3 ) ||
                                                     ( ( i == 2 ) && fila_diagonal >= i-2 && columna_diagonal >= j-2 ) ||
                                                     ( ( i == 1  ) && fila_diagonal >= i-1 && columna_diagonal >= j-1 ) )
                                                     && !stop_contar_adyacentes ){
                                                  //  cout<<"whileee"<<endl;
                                                if( ( i != 6 ) && !casilla_anterior_comprobada_1 ){

                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j+1;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                     casilla_anterior_comprobada_1 = true;
                                                     fila_diagonal = i-1;
                                                     columna_diagonal = j-1;
                                                }
                                                if( casilla_anterior_comprobada_2 && ( ( i != 6 && i != 5 ) && !casilla_anterior_comprobada_2 ) ){

                                                    fila_diagonal = i+2;
                                                    columna_diagonal = j+2;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                     casilla_anterior_comprobada_1 = true;
                                                     fila_diagonal = i-1;
                                                     columna_diagonal = j-1;
                                                }
                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                        estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) &&
                                                        !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal--;
                                                        columna_diagonal--;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal--;
                                                        columna_diagonal--;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }
                                        }

                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MIN++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"Gano en columna "<<j<< "de MIN DIAGONAL"<<endl;
                                            stop_asi_gano = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MIN++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }
                                  } //if(j == 5)
                                  else if(j == 2){

                                        if(i>=0 && i<=5){

                                            fila_diagonal = i+1;
                                            columna_diagonal = j+1;
                                            while( ( ( ( i!= 4 && i != 5 ) && (fila_diagonal <= i+3 && columna_diagonal >= j+3) ) ||
                                                     ( ( i== 4) && (fila_diagonal <= i+2 && columna_diagonal >= j+2) ) ||
                                                     ( ( i== 5) && (fila_diagonal <= i+1 && columna_diagonal >= j+1) )
                                                    ) &&
                                                   !stop_contar_adyacentes ){

                                                if( ( i != 0 ) && !casilla_anterior_comprobada_1 ){
                                                    fila_diagonal = i-1;
                                                    columna_diagonal = j-1;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                    casilla_anterior_comprobada_1 = true;
                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j+1;
                                                }
                                                if( casilla_anterior_comprobada_1 && ( ( i != 0 && i != 1 ) && !casilla_anterior_comprobada_2 ) ){
                                                    fila_diagonal = i-2;
                                                    columna_diagonal = j-2;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                    casilla_anterior_comprobada_2 = true;
                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j+1;
                                                }
                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                      estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) &&
                                                     !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal++;
                                                        columna_diagonal++;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal++;
                                                        columna_diagonal++;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }
                                        }
                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MIN++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"GANO en columna "<<j<< "de MIN DIAGONAL"<<endl;
                                            stop_asi_gano = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MIN++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }


                                        if( (i>=2 && i<=6) && !stop_asi_gano ){
                                            fila_diagonal = i-1; //incializamos la fila a una fila menos en caso de trabajar con filas de 3 a 6
                                            columna_diagonal = j+1; //volvemos a inicializar la columna
                                            casillas_libres = 0;
                                            aux_adyacente = 0;
                                            stop_contar_adyacentes = false;
                                            solo_cuenta_casillas_libres = false;
                                            casilla_anterior_comprobada_1 = false;
                                            casilla_anterior_comprobada_2 = false;
                                            while( ( ( ( i != 2 && i != 1 ) && fila_diagonal >= i-3 && columna_diagonal <= j+3 ) ||
                                                     ( ( i == 2 ) && fila_diagonal >= i-2 && columna_diagonal <= j+2 ) ||
                                                     ( ( i == 1  ) && fila_diagonal >= i-1 && columna_diagonal <= j+1 ) )
                                                     && !stop_contar_adyacentes ){
                                                  //  cout<<"whileee"<<endl;
                                                if( ( i != 6 ) && !casilla_anterior_comprobada_1 ){

                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j-1;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                     casilla_anterior_comprobada_1 = true;
                                                     fila_diagonal = i-1;
                                                     columna_diagonal = j+1;
                                                }
                                                if( casilla_anterior_comprobada_2 && ( ( i != 6 && i != 5 ) && !casilla_anterior_comprobada_2 ) ){

                                                    fila_diagonal = i+2;
                                                    columna_diagonal = j-2;
                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                     }
                                                     casilla_anterior_comprobada_1 = true;
                                                     fila_diagonal = i-1;
                                                     columna_diagonal = j+1;
                                                }
                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                        estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) &&
                                                        !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal--;
                                                        columna_diagonal++;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal--;
                                                        columna_diagonal++;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }
                                        }

                                        if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                //adyacentes_MAX += casillas_libres*10;
                                            if(aux_adyacente+1 == 1){
                                                Caminos_a_partir_de_1_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 2){
                                                Caminos_a_partir_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                Caminos_a_partir_de_3_MIN++;
                                            }
                                        }
                                        if( (aux_adyacente+1) >= 4 ){
                                            cout<<"GANO en columna "<<j<< "de MIN DIAGONAL"<<endl;
                                            stop_asi_gano = true;
                                           // cout<<"Pierdo"<<endl;
                                        }
                                        else if( aux_adyacente != 0){
                                           // adyacentes_MAX += (aux_adyacente+1)*10;
                                           // aux_adyacente = 0;
                                            if(aux_adyacente+1 == 2){
                                                adyacente_de_2_MIN++;
                                            }
                                            else if(aux_adyacente+1 == 3){
                                                adyacente_de_3_MIN++;
                                            }
                                          //  adyacentes_MAX += (aux_adyacente+1)*10;
                                            aux_adyacente = 0;
                                        }
                                  } //if(j == 2)
                        }
                    }
                    else if( j == 3  && ( !stop_asi_gano && !stop_asi_pierdo ) ){ // j == 3
                     //   cout<<"Estoy en Jugador Max , 3 "<<endl;
                        aux_adyacente = 0;
                        for(col_aux = j+1 ; col_aux<7 &&  !stop_contar_adyacentes ; col_aux++){
                            if( ( estado.See_Casilla(i,col_aux) == Jugador_Min || estado.See_Casilla(i,col_aux) == Jugador_Min+3 ) &&
                                !solo_cuenta_casillas_libres ){
                                aux_adyacente++;
                            }
                            else{
                                if(estado.See_Casilla(i,col_aux) == 0){
                                    solo_cuenta_casillas_libres=true;
                                    casillas_libres++;
                                }
                                else{
                                    stop_contar_adyacentes = true;
                                }
                            }
                        }
                        for(col_aux = j-1 ; col_aux>=0 &&  !stop_contar_adyacentes ; col_aux--){
                            if( ( estado.See_Casilla(i,col_aux) == Jugador_Min || estado.See_Casilla(i,col_aux) == Jugador_Min+3 ) &&
                                !solo_cuenta_casillas_libres ){
                                aux_adyacente++;
                            }
                            else{
                                if(estado.See_Casilla(i,col_aux) == 0){
                                    solo_cuenta_casillas_libres=true;
                                    casillas_libres++;
                                }
                                else{
                                    stop_contar_adyacentes = true;
                                }
                            }
                        }
                        if(  (aux_adyacente+1)<4 &&   (aux_adyacente+1)+casillas_libres>= 4  ){
                                //adyacentes_MAX += casillas_libres*10;
                            if(aux_adyacente+1 == 1){
                                Caminos_a_partir_de_1_MIN++;
                            }
                            else if(aux_adyacente+1 == 2){
                                Caminos_a_partir_de_2_MIN++;
                            }
                            else if(aux_adyacente+1 == 3){
                                Caminos_a_partir_de_3_MIN++;
                            }
                        }

                        if(aux_adyacente+1 >= 4){
                            cout<<"Gano en columna 3 de MIN Horizontal"<<endl;
                            stop_asi_gano = true;
                         //   cout<<"Pierdo"<<endl;
                        }
                        else if(aux_adyacente != 0){
                         //  adyacentes_MAX += (aux_adyacente+1)*10;
                            if(aux_adyacente+1 == 2){
                                adyacente_de_2_MIN++;
                            }
                            else if(aux_adyacente+1 == 3){
                                adyacente_de_3_MIN++;
                            }
                           aux_adyacente = 0;
                        }
                        if(estado.Get_Ocupacion_Columna(j) != 0){
                            i = estado.Get_Ocupacion_Columna(j) - 1 ;
                        }

                      if(!stop_asi_gano){
                            aux_adyacente = 0;
                            casillas_libres = 0;
                            if( estado.Get_Ocupacion_Columna(j) > 0)
                                i = estado.Get_Ocupacion_Columna(j) - 1 ;
                            solo_cuenta_casillas_libres = false;
                            stop_contar_adyacentes = false;
                            int fila_diagonal;
                            int columna_diagonal ;
                            bool casilla_anterior_comprobada_1 = false;
                            bool casilla_anterior_comprobada_2 = false;
                            bool casilla_anterior_comprobada_3 = false;

                                        if(i>=0 && i<=5){

                                               if(  ( i != 0 ) && !casilla_anterior_comprobada_1 ){
                                                    fila_diagonal = i-1;
                                                    columna_diagonal = j-1;
                                                    if(!solo_cuenta_casillas_libres && !stop_contar_adyacentes &&
                                                        ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                            solo_cuenta_casillas_libres = true;
                                                     }
                                                     else{
                                                        stop_contar_adyacentes = true;
                                                     }
                                                    casilla_anterior_comprobada_1 = true;

                                                }
                                                if( casilla_anterior_comprobada_1 && ( ( i != 0 && i != 1 ) && !casilla_anterior_comprobada_2 ) ){
                                                    fila_diagonal = i-2;
                                                    columna_diagonal = j-2;
                                                    if(  !solo_cuenta_casillas_libres && !stop_contar_adyacentes && ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                            solo_cuenta_casillas_libres = true;
                                                     }
                                                     else{
                                                        stop_contar_adyacentes = true;
                                                     }
                                                    casilla_anterior_comprobada_2 = true;

                                                }
                                                if( casilla_anterior_comprobada_2 && ( ( i != 0 && i !=1 && i != 2 ) && !casilla_anterior_comprobada_3 ) ){
                                                    fila_diagonal = i-3;
                                                    columna_diagonal = j-3;
                                                    if(  !solo_cuenta_casillas_libres && !stop_contar_adyacentes && ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) ){
                                                            aux_adyacente++;
                                                     }
                                                     else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            casillas_libres++;
                                                            solo_cuenta_casillas_libres = true;
                                                     }
                                                     else{
                                                        stop_contar_adyacentes = true;
                                                     }
                                                    casilla_anterior_comprobada_3 = true;

                                                }
                                                solo_cuenta_casillas_libres = false;
                                                stop_contar_adyacentes = false;
                                                fila_diagonal = i+1;
                                                columna_diagonal = j+1;

                                            while( ( ( ( i!= 4 && i != 5 ) && (fila_diagonal <= i+3 && columna_diagonal <= j+3) ) ||
                                                     ( ( i== 4) && (fila_diagonal <= i+2 && columna_diagonal <= j+2) ) ||
                                                     ( ( i== 5) && (fila_diagonal <= i+1 && columna_diagonal <= j+1) )
                                                    ) &&
                                                   !stop_contar_adyacentes ){
                                                    //   cout<<"WHILE"<<endl;


                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                      estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) &&
                                                     !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal++;
                                                        columna_diagonal++;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal++;
                                                        columna_diagonal++;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }

                                            if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                    //adyacentes_MAX += casillas_libres*10;
                                                if(aux_adyacente+1 == 1){
                                                    Caminos_a_partir_de_1_MIN++;
                                                }
                                                else if(aux_adyacente+1 == 2){
                                                    Caminos_a_partir_de_2_MIN++;
                                                }
                                                else if(aux_adyacente+1 == 3){
                                                    Caminos_a_partir_de_3_MIN++;
                                                }
                                            }
                                            if( (aux_adyacente+1) >= 4 ){
                                                cout<<"GANO en columna "<<j<< "de MIN DIAGONAL"<<endl;
                                                stop_asi_gano = true;
                                               // cout<<"Pierdo"<<endl;
                                            }
                                            else if( aux_adyacente != 0){
                                               // adyacentes_MAX += (aux_adyacente+1)*10;
                                               // aux_adyacente = 0;
                                                if(aux_adyacente+1 == 2){
                                                    adyacente_de_2_MIN++;
                                                }
                                                else if(aux_adyacente+1 == 3){
                                                    adyacente_de_3_MIN++;
                                                }
                                              //  adyacentes_MAX += (aux_adyacente+1)*10;
                                                aux_adyacente = 0;
                                            }
                                             aux_adyacente = 0;
                                            casillas_libres = 0;
                                            if( estado.Get_Ocupacion_Columna(j) > 0)
                                                i = estado.Get_Ocupacion_Columna(j) - 1 ;
                                            solo_cuenta_casillas_libres = false;
                                            stop_contar_adyacentes = false;
                                            fila_diagonal;
                                            columna_diagonal ;
                                            casilla_anterior_comprobada_1 = false;
                                            casilla_anterior_comprobada_2 = false;
                                            casilla_anterior_comprobada_3 = false;

                                                     if( ( i != 0 ) && !casilla_anterior_comprobada_1 ){
                                                        fila_diagonal = i-1;
                                                        columna_diagonal = j+1;
                                                        if( !solo_cuenta_casillas_libres && ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                              estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) ){
                                                                aux_adyacente++;
                                                         }
                                                         else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                                casillas_libres++;
                                                                solo_cuenta_casillas_libres = true;
                                                         }
                                                         else{
                                                            stop_contar_adyacentes = true;
                                                         }
                                                        casilla_anterior_comprobada_1 = true;

                                                    }
                                                    if(  casilla_anterior_comprobada_1 && ( ( i != 0 && i != 1 ) && !casilla_anterior_comprobada_2 ) ){
                                                        fila_diagonal = i-2;
                                                        columna_diagonal = j+2;
                                                        if(  !solo_cuenta_casillas_libres && !stop_contar_adyacentes && ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                              estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) ){
                                                                aux_adyacente++;
                                                         }
                                                         else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                                casillas_libres++;
                                                                solo_cuenta_casillas_libres = true;
                                                         }
                                                         else{
                                                            stop_contar_adyacentes = true;
                                                         }
                                                        casilla_anterior_comprobada_2 = true;

                                                    }
                                                    if( casilla_anterior_comprobada_2 && ( ( i != 0 && i !=1 && i != 2 ) && !casilla_anterior_comprobada_3 ) ){
                                                        fila_diagonal = i-3;
                                                        columna_diagonal = j+3;
                                                        if(  !solo_cuenta_casillas_libres && !stop_contar_adyacentes && ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                              estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) ){
                                                                aux_adyacente++;
                                                         }
                                                         else if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                                casillas_libres++;
                                                                solo_cuenta_casillas_libres = true;
                                                         }
                                                         else{
                                                            stop_contar_adyacentes = true;
                                                         }
                                                        casilla_anterior_comprobada_3 = true;

                                                    }
                                                    solo_cuenta_casillas_libres = false;
                                                    stop_contar_adyacentes = false;
                                                    fila_diagonal = i+1;
                                                    columna_diagonal = j-1;
                                            while( ( ( ( i!= 4 && i != 5 ) && (fila_diagonal <= i+3 && columna_diagonal >= j-3) ) ||
                                                         ( ( i== 4) && (fila_diagonal <= i+2 && columna_diagonal >= j-2) ) ||
                                                         ( ( i== 5) && (fila_diagonal <= i+1 && columna_diagonal >= j-1) )
                                                        ) &&
                                                       !stop_contar_adyacentes ){
                                                        //   cout<<"WHILE"<<endl;


                                                    if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                          estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) &&
                                                         !solo_cuenta_casillas_libres)
                                                    {
                                                            aux_adyacente++;
                                                            fila_diagonal++;
                                                            columna_diagonal--;

                                                    }
                                                    else{
                                                        if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                            solo_cuenta_casillas_libres = true;
                                                            casillas_libres++;

                                                            fila_diagonal++;
                                                            columna_diagonal--;
                                                        }
                                                        else{
                                                            stop_contar_adyacentes = true;
                                                        }
                                                    }

                                                }

                                            if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                    //adyacentes_MAX += casillas_libres*10;
                                                if(aux_adyacente+1 == 1){
                                                    Caminos_a_partir_de_1_MIN++;
                                                }
                                                else if(aux_adyacente+1 == 2){
                                                    Caminos_a_partir_de_2_MIN++;
                                                }
                                                else if(aux_adyacente+1 == 3){
                                                    Caminos_a_partir_de_3_MIN++;
                                                }
                                            }
                                            if( (aux_adyacente+1) >= 4 ){
                                                cout<<"gano en columna "<<j<< "de MIN DIAGONAL"<<endl;
                                                stop_asi_gano = true;
                                               // cout<<"Pierdo"<<endl;
                                            }
                                            else if( aux_adyacente != 0){
                                               // adyacentes_MAX += (aux_adyacente+1)*10;
                                               // aux_adyacente = 0;
                                                if(aux_adyacente+1 == 2){
                                                    adyacente_de_2_MIN++;
                                                }
                                                else if(aux_adyacente+1 == 3){
                                                    adyacente_de_3_MIN++;
                                                }
                                              //  adyacentes_MAX += (aux_adyacente+1)*10;
                                                aux_adyacente = 0;
                                            }
                                    }
                                    else if( i == 6){
                                             fila_diagonal = i-1;
                                            columna_diagonal = j+1;
                                            while( (fila_diagonal >= i-3 && columna_diagonal <= j+3) && !stop_contar_adyacentes ){
                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                      estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) &&
                                                     !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal--;
                                                        columna_diagonal++;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal--;
                                                        columna_diagonal++;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }

                                            if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                    //adyacentes_MAX += casillas_libres*10;
                                                if(aux_adyacente+1 == 1){
                                                    Caminos_a_partir_de_1_MIN++;
                                                }
                                                else if(aux_adyacente+1 == 2){
                                                    Caminos_a_partir_de_2_MIN++;
                                                }
                                                else if(aux_adyacente+1 == 3){
                                                    Caminos_a_partir_de_3_MIN++;
                                                }
                                            }
                                            if( (aux_adyacente+1) >= 4 ){
                                                cout<<"Gano en columna "<<j<< "de MIN DIAGONAL"<<endl;
                                                stop_asi_gano = true;
                                               // cout<<"Pierdo"<<endl;
                                            }
                                            else if( aux_adyacente != 0){
                                               // adyacentes_MAX += (aux_adyacente+1)*10;
                                               // aux_adyacente = 0;
                                                if(aux_adyacente+1 == 2){
                                                    adyacente_de_2_MAX++;
                                                }
                                                else if(aux_adyacente+1 == 3){
                                                    adyacente_de_3_MAX++;
                                                }
                                              //  adyacentes_MAX += (aux_adyacente+1)*10;
                                                aux_adyacente = 0;
                                            }
                                            fila_diagonal = i-1;
                                            columna_diagonal = j-1;
                                            while( (fila_diagonal >= i-3 && columna_diagonal >= j-3) && !stop_contar_adyacentes ){

                                                if( ( estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min ||
                                                      estado.See_Casilla(fila_diagonal,columna_diagonal) == Jugador_Min+3 ) &&
                                                     !solo_cuenta_casillas_libres)
                                                {
                                                        aux_adyacente++;
                                                        fila_diagonal--;
                                                        columna_diagonal--;

                                                }
                                                else{
                                                    if( estado.See_Casilla(fila_diagonal,columna_diagonal) == 0 ){
                                                        solo_cuenta_casillas_libres = true;
                                                        casillas_libres++;

                                                        fila_diagonal--;
                                                        columna_diagonal--;
                                                    }
                                                    else{
                                                        stop_contar_adyacentes = true;
                                                    }
                                                }

                                            }

                                            if(  (aux_adyacente+1)<4 &&  (aux_adyacente+1)+casillas_libres>= 4  ){
                                                    //adyacentes_MAX += casillas_libres*10;
                                                if(aux_adyacente+1 == 1){
                                                    Caminos_a_partir_de_1_MIN++;
                                                }
                                                else if(aux_adyacente+1 == 2){
                                                    Caminos_a_partir_de_2_MIN++;
                                                }
                                                else if(aux_adyacente+1 == 3){
                                                    Caminos_a_partir_de_3_MIN++;
                                                }
                                            }
                                            if( (aux_adyacente+1) >= 4 ){
                                                cout<<"GANO en columna "<<j<< "de MIN DIAGONAL"<<endl;
                                                stop_asi_gano = true;
                                               // cout<<"Pierdo"<<endl;
                                            }
                                            else if( aux_adyacente != 0){
                                               // adyacentes_MAX += (aux_adyacente+1)*10;
                                               // aux_adyacente = 0;
                                                if(aux_adyacente+1 == 2){
                                                    adyacente_de_2_MIN++;
                                                }
                                                else if(aux_adyacente+1 == 3){
                                                    adyacente_de_3_MIN++;
                                                }
                                              //  adyacentes_MAX += (aux_adyacente+1)*10;
                                                aux_adyacente = 0;
                                            }

                                    }
                            }

                    }
                   if(estado.Get_Ocupacion_Columna(j) != 0){

                        i = estado.Get_Ocupacion_Columna(j) - 1 ;
                   }
                   aux_adyacente = 0;
              }
        }

    }

    if(! stop_asi_gano && ! stop_asi_pierdo){
        /* Calculo Final de Heuristica */
        Parte_Activo = ( Caminos_a_partir_de_1_MAX * 25 + Caminos_a_partir_de_2_MAX * 50 + Caminos_a_partir_de_3_MAX * 75 )+( adyacente_de_2_MAX * 50 + adyacente_de_3_MAX * 75 ) ;
        /* Si hay mas caminos que se pueden completar a partir de 3 casillas adyacentes que caminos que se pueden completar a partir de 2 casillas adyacentes
           Esto implica una mayor penalizaci�n .
        */

        if(Caminos_a_partir_de_3_MAX > Caminos_a_partir_de_2_MAX){
            Parte_Activo += ( Caminos_a_partir_de_3_MAX - Caminos_a_partir_de_2_MAX ) * 10;
        }

        Parte_Adversario = ( Caminos_a_partir_de_1_MIN * 25 + Caminos_a_partir_de_2_MIN * 50 + Caminos_a_partir_de_3_MIN * 75 )+( adyacente_de_2_MIN * 50 + adyacente_de_3_MIN * 75 ) ;
        /* Si hay mas caminos que se pueden completar a partir de 3 casillas adyacentes que caminos que se pueden completar a partir de 2 casillas adyacentes
           Esto implica una mayor penalizaci�n .
        */

        if(Caminos_a_partir_de_3_MIN > Caminos_a_partir_de_2_MIN ){
            Parte_Adversario += ( Caminos_a_partir_de_3_MIN - Caminos_a_partir_de_2_MIN ) * 10;
        }

        resultado = Parte_Adversario - Parte_Activo ;
    }
    else if(stop_asi_gano == true){
/*
        for(int k = 0 ; k<7 ; k++){
            for(int s = 0 ; s<7 ; s++){
                int aux = estado.See_Casilla(k,s);
                cout<<aux<<" ";
            }
            cout<<endl;
        }
*/
        resultado = 5000000.0;
        cout<<"Gano"<<endl;
    }
    else if(stop_asi_pierdo == true){
      //  cout<<"PIERDO"<<endl;
/*
        for(int k = 0 ; k<7 ; k++){
            for(int s = 0 ; s<7 ; s++){
                int aux = estado.See_Casilla(k,s);
                cout<<aux<<" ";
            }
            cout<<endl;
        }
*/
        resultado = -5000000.0;
        cout<<"Pierdo"<<endl;
    //    cout<<"adyacentes MIN"<<adyacentes_MIN<<" ... "<<"adya_MAX"<<adyacentes_MAX<<endl;

    }

    if(estado.Have_BOOM(Jugador_Min) == true){
        int ult_act = 6;
       // cout<<"ANTES"<<endl;
        Environment hijo = estado.GenerateNextMove(ult_act); // Genero el siguiente tablero
       /*
        for(int k = 0 ; k<7 ; k++){
            for(int s = 0 ; s<7 ; s++){
                int aux = hijo.See_Casilla(k,s);
                cout<<aux<<" ";
            }
            cout<<endl;
        }

       cout<<"*********** * * * * * * * * * * * * **************"<<endl;
            cout<<"asdada"<<endl;
*/
        int resultado_auxiliar = Heuristica(hijo,Jugador_Min);
       // cout<<"DESPUES"<<endl;
        if( resultado_auxiliar > resultado){
            resultado = -10000000.0;

        }
    }

   // cout<<adyacentes_MAX<<endl;
   // cout<<"Gano"<<stop_asi_gano<<"  , Pierdo"<<stop_asi_pierdo<<endl;
  //  cout<<"resultado"<<resultado<<endl;

    return resultado;
}




// Esta funcion no se puede usar en la version entregable
// Aparece aqui solo para ILUSTRAR el comportamiento del juego
// ESTO NO IMPLEMENTA NI MINIMAX, NI PODA ALFABETA
void JuegoAleatorio(bool aplicables[], int opciones[], int &j){
    j=0;
    for (int i=0; i<8; i++){
        if (aplicables[i]){
           opciones[j]=i;
           j++;
        }
    }
}

double Valoracion(const Environment &estado, int jugador)
{
    int ganador = estado.RevisarTablero();

    if (ganador==jugador)
       return 99999999.0; // Gana el jugador que pide la valoracion
    else if (ganador!=0)
            return -99999999.0; // Pierde el jugador que pide la valoracion
    else if (estado.Get_Casillas_Libres()==0)
            return 0;  // Hay un empate global y se ha rellenado completamente el tablero
    else
          return Heuristica(estado , jugador);
}

double Player::PODA_ALFA_PETA(const Environment &tablero,int jug,int prof,int limite,Environment::ActionType &accion,double alpha,double beta){
  const double INF = 1000000000.0, mINF = -1000000000.0, gano = 99999999, pierdo = -99999999;
  Environment::ActionType accion_anterior;
  //Environment::ActionType accion_anterior;
  bool opciones[8];
  double mejor, aux;

  if (prof==limite || tablero.JuegoTerminado() ){ // Condicion de parada
   //   cout<<Valoracion(tablero,jug)<<endl;

      return Valoracion(tablero,jug);
  }
  else  { // Proceso recursivo de calculo de MiniMax
      int n_act = tablero.possible_actions(opciones); // Determino cuantas acciones se pueden realizar


      if (n_act==0){ // Si no se puede realizar ninguna acci�n
          if (prof%2==0) // En los niveles pares juega MAX
             return pierdo; // Yo no puedo mover y entonces pierdo
          else
             return gano;   // El que n puede mover es el adversario y gano
      }

      // Inicializo el mejor valor en funcion del nivel

      if (prof%2==0)
        mejor = mINF; // Inicializar un nivel MAX
      else
        mejor = INF;  // Inicializar un nivel MIN


      int ult_act=-1;
      Environment hijo = tablero.GenerateNextMove(ult_act); // Genero el siguiente tablero

      while (!(hijo==tablero)){ // Condicion de que sigo pudiendo aplicar nuevas acciones al tablero
          aux = PODA_ALFA_PETA(hijo,jug,prof+1,limite,accion_anterior,alpha,beta); // Evaluo el valor del hijo

          if (prof%2==0){ // Juega MAX
              if (aux>mejor){
                  mejor=aux;
                  if(mejor>=alpha){
                    alpha=mejor;
                  }
                  accion = static_cast <Environment::ActionType > (ult_act);

              }
              if(mejor>=beta){//poda BETA
                    hijo=tablero;
              }
              else{
                    hijo=tablero.GenerateNextMove(ult_act);
             }
          }
          else {         // Juega MIN
              if (aux<mejor){
                  mejor=aux;
                  if(mejor<=beta){
                    beta=mejor;
                  }
                  accion = static_cast <Environment::ActionType > (ult_act);

              }
              if(mejor<=alpha){//poda ALFA
                    hijo=tablero;
              }
              else{
                    hijo=tablero.GenerateNextMove(ult_act);
              }
          }
      }
     // cout<<mejor<<endl;
      return mejor;

  }


}



// Invoca el siguiente movimiento del jugador

/*
Environment::ActionType Player::Think(){
    const int PROFUNDIDAD_MINIMAX = 10;  // Umbral maximo de profundidad para el metodo MiniMax
    const int PROFUNDIDAD_ALFABETA = 8; // Umbral maximo de profundidad para la poda Alfa_Beta

    Environment::ActionType accion; // acci�n que se va a devolver
    bool aplicables[8];

    double valor; // Almacena el valor con el que se etiqueta el estado tras el proceso de busqueda.
    double alpha, beta; // Cotas de la poda AlfaBeta

    int n_act; //Acciones posibles en el estado actual


    n_act = actual_.possible_actions(aplicables); // Obtengo las acciones aplicables al estado actual en "aplicables"
    int opciones[8];

    // Muestra por la consola las acciones aplicable para el jugador activo
    cout << " Acciones aplicables para Jugador ";
    (jugador_==1) ? cout << "Verde: " : cout << "Azul: ";
    for (int t=0; t<n_act; t++)
      if (aplicables[t])
         cout << " " << actual_.ActionStr( static_cast< Environment::ActionType > (t)  );
    cout << endl;


    //--------------------- AQUI EMPIEZA LA PARTE A REALIZAR POR EL ALUMNO ------------------------------------------------
  //  antes=actual_;
    alpha=-100000000.0;
    beta=100000000.0;

    valor = PODA_ALFA_PETA(actual_, jugador_, 0,PROFUNDIDAD_ALFABETA, accion, alpha, beta);
    cout << "Valor alfaBeta: " << valor << "  Accion: " << actual_.ActionStr(accion) << endl;
    cout<<"JUGADOR"<<jugador_<<endl;

    return accion;
}
*/
Environment::ActionType Player::Think(){

    const int PROFUNDIDAD_MINIMAX = 6;  // Umbral maximo de profundidad para el metodo MiniMax
    const int PROFUNDIDAD_ALFABETA = 8; // Umbral maximo de profundidad para la poda Alfa_Beta

    Environment::ActionType accion; // acci�n que se va a devolver
    bool aplicables[8];




    double valor; // Almacena el valor con el que se etiqueta el estado tras el proceso de busqueda.
    double alpha, beta; // Cotas de la poda AlfaBeta

    int n_act; //Acciones posibles en el estado actual


    n_act = actual_.possible_actions(aplicables); // Obtengo las acciones aplicables al estado actual en "aplicables"
    int opciones[8];

    // Muestra por la consola las acciones aplicable para el jugador activo
    //actual_.PintaTablero();
    cout << " Acciones aplicables ";
    (jugador_==1) ? cout << "Verde: " : cout << "Azul: ";
    for (int t=0; t<8; t++)
      if (aplicables[t])
         cout << " " << actual_.ActionStr( static_cast< Environment::ActionType > (t)  );
    cout << endl;



    // Opcion: Poda AlfaBeta
    // NOTA: La parametrizacion es solo orientativa
     valor = PODA_ALFA_PETA(actual_, jugador_, 0, PROFUNDIDAD_ALFABETA, accion, alpha, beta);
     cout << "Valor MiniMax: " << valor << "  Accion: " << actual_.ActionStr(accion) << endl;

    return accion;
  /*

    const int PROFUNDIDAD_MINIMAX = 6;  // Umbral maximo de profundidad para el metodo MiniMax
    const int PROFUNDIDAD_ALFABETA = 8; // Umbral maximo de profundidad para la poda Alfa_Beta

    Environment::ActionType accion; // acci�n que se va a devolver
    bool aplicables[8]; // Vector bool usado para obtener las acciones que son aplicables en el estado actual. La interpretacion es




    double valor; // Almacena el valor con el que se etiqueta el estado tras el proceso de busqueda.
    double alpha, beta; // Cotas de la poda AlfaBeta

    int n_act; //Acciones posibles en el estado actual


    n_act = actual_.possible_actions(aplicables); // Obtengo las acciones aplicables al estado actual en "aplicables"
    int opciones[10];

    // Muestra por la consola las acciones aplicable para el jugador activo
    //actual_.PintaTablero();
    cout << " Acciones aplicables ";
    (jugador_==1) ? cout << "Verde: " : cout << "Azul: ";
    for (int t=0; t<8; t++)
      if (aplicables[t])
         cout << " " << actual_.ActionStr( static_cast< Environment::ActionType > (t)  );
    cout << endl;


    //--------------------- AQUI EMPIEZA LA PARTE A REALIZAR POR EL ALUMNO ------------------------------------------------
     valor = PODA_ALFA_PETA(actual_, jugador_, 0, PROFUNDIDAD_ALFABETA, accion, alpha, beta);

    cout << "Valor MiniMax: " << valor << "  Accion: " << actual_.ActionStr(accion) << endl;

    return accion;
    */

}

